package groupofthree.taskrabbit.content;

import java.security.Key;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by kyriakos on 10/12/16.
 */

/**
 * Class that implements a storage for temporary storing Date values for keeping track of time in lists.
 */
public class DateStorage {

    private static DateStorage self = new DateStorage();
    private static HashMap<String, Date> map = new HashMap<>();
    private DateStorage(){}

    public static DateStorage getInstance(){
        if (self == null){                      // this should never happen
            self = new DateStorage();
            map = new HashMap<>();
        }

        return self;
    }

    /**
     * Store a date associated with its key. This method will not save a value for a key that already exists.
     * @param key the key to associate the value with
     * @param dateValue the value to be stored
     * @return *true* if the key was not previously stored, *false* otherwise.
     */
    public boolean put(String key, Date dateValue){
        if (map.containsKey(key)) return false;

        map.put(key, dateValue);
        return true;

    }

    /**
     * Get the value associated with the key. After this call, the key-value pair is removed from storage.
     * @param key the key of the key-value pair
     * @return the value of the date stored
     */
    public Date get(String key){
        return map.remove(key);
    }

    public boolean valueExists(String key){
        return map.containsKey(key);
    }

    public boolean isEmpty(){
        return map.isEmpty();
    }

    public void clear(){
        map.clear();
    }

}
