package groupofthree.taskrabbit;

import android.support.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;

import android.location.Location;
import android.support.test.*;
import android.util.Log;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import groupofthree.taskrabbit.content.ContentManager;
import groupofthree.taskrabbit.content.DateStorage;
import groupofthree.taskrabbit.content.database.DataBaseHelper;
import groupofthree.taskrabbit.entities.TodoList;
import groupofthree.taskrabbit.entities.location.LocationWithTag;
import groupofthree.taskrabbit.entities.Task;
import groupofthree.taskrabbit.utils.DurationConverter;
import groupofthree.taskrabbit.utils.MinutesHoursDays;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 *
 * NOTE: THOSE TESTS WERE MADE FOR A PREVIOUS VERSION OF THE API AND PROBABLY THEY WILL NOT PASS NOW
 */
@RunWith(AndroidJUnit4.class)
public class DatabaseTest {

    private void _assert(boolean expression){
        if (!expression) throw new AssertionError();
    }

    DateStorage storage;

    @Before
    public void init(){
        storage = DateStorage.getInstance();

    }

    public void clearStorage(){

        DateStorage.getInstance().clear();
        _assert(DateStorage.getInstance().isEmpty());
    }

    @Test
    public void checkInsertion(){

        clearStorage();

        Date now_prev = new Date();
        storage.put("now", now_prev);
        _assert(!storage.isEmpty());

        Date now_after = storage.get("now");
        _assert(now_after != null);
        _assert(storage.isEmpty());

        Log.d("__test", "previous date: "+now_prev.getTime());
        Log.d("__test", "current date: "+now_after.getTime());
    }

    @Test
    public void InsertAndWait(){

        long WAIT_PERIOD = 100000000,
        x=0;
        clearStorage();

        Date now1 = new Date();
        storage.put("now1", now1);
        _assert(!storage.isEmpty());

        for (long i=0; i<WAIT_PERIOD; i++){
            x++;
        }

        Date now2 = new Date();
        storage.put("now2", now2);
        _assert(!storage.isEmpty());

        for (long i=0; i<WAIT_PERIOD; i++){
            x++;
        }

        Log.d("__test", "stored:\t"+now1.getTime()+"\t"+now2.getTime());

    }


    @Test
    public void retrieveInsertions(){

        _assert(!storage.isEmpty());

        Date first = storage.get("now1");
        Date second = storage.get("now2");
        _assert(storage.isEmpty());

        Log.d("__test", "retrieved:\t"+first.getTime()+"\t"+second.getTime());

    }


    @Test
    public void countTime() throws InterruptedException{

        Sleeper sleeper = new Sleeper(2000);
        Date prev = new Date();
        sleeper.sleep();
        Date after = new Date();

        MinutesHoursDays mhd = DurationConverter.getTimeDuration( after.getTime() - prev.getTime());
        Log.d("__test", "duration: "+mhd.toString());





    }

    @Test
    public void checkDate(){

        Location loc = new Location("");
        loc.setLatitude(1);
        loc.setLongitude(1);

        TodoList l = new TodoList(loc, "l");

        Task t = new Task("a task", new Date(), loc);

        Log.d("test", "");

        l.addTask(t);

        ContentManager.getInstance(InstrumentationRegistry.getTargetContext()).saveTodoList(l);
        ArrayList<Task> tasks = ContentManager.getInstance(InstrumentationRegistry.getTargetContext()).getTasksByLocation(1,1, 100);
        Task t2 = tasks.get(0);
        Log.d("test", t2.getDateCreated().toString());


    }


    public void seed()throws Exception{


        boolean __skip = true;



        if (__skip) return;

        clearStorage();


        Location usi = new Location("");
        usi.setLatitude(46.0109603);
        usi.setLongitude(8.9582572);
        Task taskAtUsi = new Task("Atelier Project", usi);
        TodoList listAtUsi = new TodoList(usi, "projects");
        listAtUsi.addTask(taskAtUsi);

        Location usiHome = new Location("");
        usiHome.setLatitude(46.0079987);
        usiHome.setLongitude(8.9568776);
        Task taskAtUsiHome = new Task("Clean kitchen", usiHome);
        Task taskAtUsiHome2 = new Task("Cooking", usiHome);
        TodoList listAtUsiHome = new TodoList(usiHome, "chores");
        listAtUsiHome.addTask(taskAtUsiHome);
        listAtUsiHome.addTask(taskAtUsiHome2);

        TodoList listAtUsi2 = new TodoList(usi, "assignments");
        listAtUsi2.setLocationTag("university");
        listAtUsi2.addTask(new Task("PF3", usi));

        listAtUsi.setTimeSpent(10000005272L);
        listAtUsi2.setTimeSpent(87409272L);
        listAtUsiHome.setTimeSpent(7202810027L);

        ContentManager contentManager = ContentManager.getInstance(InstrumentationRegistry.getTargetContext());
        contentManager.saveTodoList(listAtUsi);
        contentManager.saveTodoList(listAtUsi2);
        contentManager.saveTodoList(listAtUsiHome);

    }

    @Test
    public void checkDeleteion(){

        ContentManager contentManager = ContentManager.getInstance(InstrumentationRegistry.getTargetContext());

        Location usi = new Location("");
        usi.setLatitude(46.0109603);
        usi.setLongitude(8.9582572);
        Task taskAtUsi = new Task("Atelier Project", usi);
        TodoList listAtUsi = new TodoList(usi, "projects");
        listAtUsi.addTask(taskAtUsi);

        contentManager.saveTodoList(listAtUsi);

        _assert(contentManager.getAllLists().size() == 1);
        _assert(contentManager.getAllTasks().size() == 1);


        contentManager.deleteTodoList(listAtUsi);

        _assert(contentManager.getAllLists().size() == 0);
        _assert(contentManager.getAllTasks().size() == 0);

    }






}
