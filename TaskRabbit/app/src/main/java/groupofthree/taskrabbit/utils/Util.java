package groupofthree.taskrabbit.utils;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by kyriakos on 17/12/16.
 */

/**
 * A bundle of static methods that are needed in different parts of the app
 * Keeping them here makes code more organised, plus it permits code reuse
 */
public class Util {

    private Util(){}

    /**
     * Calculate the percentage value of each element of the list, in regard of the total sum of the elements
     * @param vals A list of values whose percentages will be calculated
     * @return An ArrayList with the percentage values of the elements of the input list (order is guaranteed to be the same as the initial list)
     */
    public static ArrayList<Float> calculatePercentages(List<? extends Long> vals){


        float total = 0;
        for (long v: vals) total = total + v;


        ArrayList<Float> percentages = new ArrayList<>();

        // for loop is needed cause foreach / iterators do not guarantee order.
        for (int i = 0; i<vals.size(); i++){
            float percent = (vals.get(i) / total)*100;
            percentages.add(percent);
        }

        return percentages;
    }

    /**
     * Given a list of elements, randomly rearrange its elements
     * @param list The list to be rearranged
     * @param <T> The type of the elements
     * @return An ArrayList with the elements of the list in a random order
     *
     * The input list and its elements remain immutable
     */
    public static <T> ArrayList<T> randomiseElementsOrder(List<T> list){

        List newList = list;
        ArrayList<T> randomised = new ArrayList<>(newList.size());

        while (newList.size() > 0){
            randomised.add( list.remove( randInt(newList.size()) ) );
        }

        return randomised;
    }

    public static int randInt(int max){
        int i = new Random().nextInt();
        i = ( i < 0 ) ? i*(-1) : i;
        return i % max;
    }

    /**
     * Compute the average value of an array
     * @param values The array of values to compute their average
     * @return the average value
     */
    public static double average(float[] values){
        double sum = 0;
        for (float v : values) sum+=v;
        return sum / values.length;
    }

    /**
     * Compute the variance value
     * @param values
     * @return
     */
    public static double variance(float[] values){
        // s = Σ( (x_i)^2 - average^2 )
        double average = average(values);
        double sum = 0;

        for (float v : values) sum += (v*v - average*average);

        return sum;

    }

}
