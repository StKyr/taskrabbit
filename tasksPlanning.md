# Alocated Tasks

### Fabian
- Listfragment
	- Task can be finished 
	- Task could have subtasks
	- To location (later)
	- As Fragment
- List of Lists
- UI Assets \ Mockup
	- Listfragment (what happens with finished tasks)
	- Dashboard
	- Detailed View / Detailed creation

### Kyriakos
- Add Task

### Robert
- Map
	- Name of List
	- Marker of Location
	- Distance to current Location
	- Add current Location to new task


# Open TODOS:

## High Prioraty

- Add Task in Detail / Detail View
- Dashboard Screen


## Medium Priority
- Statics Visualisation
	- Bar Chart
	- Pie Chart

## Low Priority


# Finished Tasks

- Basic classes / Dummy content manager
- Database
- Notification
- Track of Time

