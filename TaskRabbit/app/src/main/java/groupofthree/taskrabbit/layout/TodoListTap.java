package groupofthree.taskrabbit.layout;

/**
 * Created by Fabian on 15/12/16.
 */

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.maps.model.Marker;

import groupofthree.taskrabbit.R;

import groupofthree.taskrabbit.layout.statistics.StatisticsTab;

/**
 Created by Fabian Ramelsberger
 **/

public class TodoListTap extends Fragment {


    private TextView nameText;
    private TextView metersText;
    private TextView timeSpentText;
    private EditText editTextNewTodo;
    private Marker currentMarker;
    private float distanceToLocation;

    public TodoListTap() {
        // Required empty public constructor
    }

    public static StatisticsTab newInstance() {
        StatisticsTab fragment = new StatisticsTab();
        fragment.setArguments(null);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_todolist_tap, container, false);
        nameText = (TextView) view.findViewById(R.id.todo_list_name);
        metersText = (TextView) view.findViewById(R.id.todo_list_meters);
        timeSpentText = (TextView) view.findViewById(R.id.timeSpentTextView);
        editTextNewTodo = (EditText) view.findViewById(R.id.EditEmptyTodo);
        return view;
    }

    public void onSetNameAndDistance(String name, String distance) {
        nameText.setText(name);
        metersText.setText(distance);
    }

    public EditText getEditTextString() {
        return editTextNewTodo;

    }

    public void onSetTimeSpentOn(String timeSpend) {
        timeSpentText.setText(timeSpend);
    }


    public void updateInnerList(Marker marker, float distanceToLocation) {
        this.currentMarker=marker;
        this.distanceToLocation=distanceToLocation;
    }

    public Marker getCurrentMarker() {
        return currentMarker;
    }

    public float getDistanceToLocation() {
        return distanceToLocation;
    }
}
