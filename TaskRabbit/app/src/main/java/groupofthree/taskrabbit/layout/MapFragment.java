package groupofthree.taskrabbit.layout;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.ActivityRecognition;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.maps.android.ui.IconGenerator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import groupofthree.taskrabbit.R;
import groupofthree.taskrabbit.content.ContentManager;
import groupofthree.taskrabbit.entities.TodoList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MapFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MapFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MapFragment extends Fragment implements GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks, com.google.android.gms.location.LocationListener, OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    // Map
    private MapView mapView;
    private GoogleMap mMap;


    // Map positions
    private static final String KEY_CAMERA_POSITION = "camera_position";
    private static final String KEY_LOCATION = "location";

    private Location mCurrentLocation;
    private CameraPosition mCameraPosition;

    private LatLng mDefaultLocation;
    private static final int DEFAULT_ZOOM = 20;

    // Google map api
    private GoogleApiClient mGoogleApiClient;
    boolean mLocationPermissionGranted;
    private LocationRequest mLocationRequest;

    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 0;

    // Json request
    private RequestQueue requestQueue;

    // Fragment refrences
    private Context currContext;
    private FragmentActivity currActivity;

    private LayoutInflater fragmentInflater;
    private View currView;

    private OnFragmentInteractionListener mListener;



    public MapFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment MapFragment.
     */

    public static MapFragment newInstance(CameraPosition prevCameraLoc, Location prevLoc) {
        MapFragment fragment = new MapFragment();
        Bundle args = new Bundle();
        args.putParcelable(KEY_LOCATION, prevLoc);
        args.putParcelable(KEY_CAMERA_POSITION, prevCameraLoc);
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Set location and camera state
     **/
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        // Save the user's current game state

        if(mMap != null) {

            savedInstanceState.putParcelable(KEY_CAMERA_POSITION, mCameraPosition);

            if(mCurrentLocation == null){
                savedInstanceState.putParcelable(KEY_LOCATION, mDefaultLocation);
            }
            else{
                savedInstanceState.putParcelable(KEY_LOCATION,mCurrentLocation);
            }
        }
        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_map_tap, container, false);

        fragmentInflater = inflater; // Save reference

        mapView = (MapView) v.findViewById(R.id.mapview);
        mapView.onCreate(savedInstanceState);

        currView = v;

        return v;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        // Set defaults
        mDefaultLocation = new LatLng(41.0, 50.1);

        // Check for saved instances
        if (savedInstanceState != null) {
            mCurrentLocation = savedInstanceState.getParcelable(KEY_LOCATION);
            mCameraPosition = savedInstanceState.getParcelable(KEY_CAMERA_POSITION);
        }
        else if(getArguments() != null){
            mCurrentLocation = getArguments().getParcelable(KEY_LOCATION);
            mCameraPosition = getArguments().getParcelable(KEY_CAMERA_POSITION);
        }
        else {
            MainActivity activity = (MainActivity)getActivity();
            if(activity.loc != null){
                mCurrentLocation = activity.loc;
            }

            if(activity.camPos != null){
                mCameraPosition = activity.camPos;
            }
        }


        super.onCreate(savedInstanceState);

        // set references
        currActivity = getActivity();
        currContext = getContext();

        // Setup Google Api Client
        if (mGoogleApiClient == null) {
            buildGoogleApiClient();
        }
        if (mLocationRequest == null) {
            createLocationRequest();
        }

        mGoogleApiClient.connect();

    }

    /**
     * Init GoogleApiClient object
     **/

    private synchronized void buildGoogleApiClient() {


        mGoogleApiClient = new GoogleApiClient.Builder(currContext)
                .addConnectionCallbacks(this)
                .addApi(LocationServices.API)
                .build();
    }

    /**
     * Init Location Request object
     **/

    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(5000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_LOW_POWER);
    }

    /**
     * GoogleApiClient methods
     **/

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(currContext, "GoogleApiClient connection error: " + connectionResult, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        // JSON request que init
        if (requestQueue == null) {
            requestQueue = Volley.newRequestQueue(currContext.getApplicationContext());
        }
        else{
            requestQueue.start();
        }

        // Get map element
        mapView.getMapAsync(this);

    }

    @Override
    public void onConnectionSuspended(int i) {
        Toast.makeText(currContext, "GoogleApiClient suspended", Toast.LENGTH_SHORT).show();
    }

    /**
     * Init map element
     **/

    @Override
    public void onMapReady(GoogleMap map) {

        mMap = map;
        if(mCameraPosition != null){
        MapsInitializer.initialize(currActivity);

        CameraUpdate camLoc = CameraUpdateFactory.newLatLngZoom(new LatLng(mCameraPosition.target.latitude, mCameraPosition.target.longitude), mCameraPosition.zoom);

        mMap.moveCamera(camLoc);

        }

        mMap.setOnCameraMoveListener(new GoogleMap.OnCameraMoveListener() {
            @Override
            public void onCameraMove() {
                mCameraPosition = mMap.getCameraPosition();
            }
        });
        mMap.setOnMarkerClickListener(this);

        getDeviceLocation();

    }

    /**
     * Marker click listener
     **/

    @Override
    public boolean onMarkerClick(Marker marker) {

        MainActivity activity = (MainActivity)currActivity;

        // Check if delete mode enabled
        if(activity.inDeletionMapMode){

            ContentManager cm = ContentManager.getInstance(currContext);

            ArrayList<TodoList> lists = cm.getAllLists();
            for(TodoList list : lists)
            {
                if(list.getName().equals(marker.getTitle())){

                    cm.deleteTodoList(list);
                    marker.remove();

                    break;

                }
            }

        // Ohterwise switch to list view
        }else{
            Location location = new Location(marker.getTitle());

            location.setLatitude(marker.getPosition().latitude);
            location.setLongitude(marker.getPosition().longitude);

            // Pass distance and marker data
            float distanceToLocation = 0;
            if (mCurrentLocation != null)
                distanceToLocation = mCurrentLocation.distanceTo(location);

            ((MainActivity) getActivity()).updateCurrentList(marker, distanceToLocation);
        }

        return true;

    }

    /**
     * Ask for permission for device location
     **/

    private void getDeviceLocation() {

        if (Build.VERSION.SDK_INT >= 23) {

            if (ContextCompat.checkSelfPermission(currActivity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    || ContextCompat.checkSelfPermission(currActivity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)) {
                    Toast.makeText(currContext, "Please enable GPS permissions", Toast.LENGTH_SHORT);
                }
            } else {
                requestPermissions(
                        new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                        PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
            }

        } else {
            // Add support for older phones
            Toast.makeText(currContext, "Api < 23", Toast.LENGTH_SHORT);
        }

        // Start location monitoring
        if (mLocationPermissionGranted) {
            mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);

        }

    }

    /**
     * Permission request result
     **/

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        mLocationPermissionGranted = false;
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mLocationPermissionGranted = true;
                }
            }

            // update ui for current location
            updateLocationUI();
        }
    }


    /**
     * Add current location buttons if location is enabled
     **/

    private void updateLocationUI() {
        if (mMap == null) {
            return;
        }

        try {
            if (mLocationPermissionGranted) {
                mMap.setMyLocationEnabled(true);


                setOnLongPress();
                mMap.getUiSettings().setMyLocationButtonEnabled(true);
            } else {
                mMap.setMyLocationEnabled(false);
                mMap.getUiSettings().setMyLocationButtonEnabled(false);
                mCurrentLocation = null;
            }

        } catch (SecurityException ex) {
            Toast.makeText(currContext, "Security exeption", Toast.LENGTH_SHORT);
        }

        // Update markers on the map
        updateMarkers();
    }

    /**
     * Long press handler used for adding tasklists
     **/

    private void setOnLongPress() {
        mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {


            @Override
            public void onMapLongClick(LatLng latLng) {

                MainActivity activity = (MainActivity)currActivity;

                // Do not att lists in delete mode
                if(!activity.inDeletionMapMode) {

                    // Get selected place information
                    getPlaces(latLng);
                }
            }

        });

    }

    /**
     * Make json request to get place information
     **/

    void getPlaces(final LatLng location) {

        String key = "AIzaSyDm6yGCQ8WYe0m58xEdnvrukK6oINXhfB8";

        String url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + location.latitude + "," + location.longitude + "&rankby=distance" +"&key=" + key;
        Log.i("URL", url);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                        String name = null;

                        if (response != null) {
                            Gson gson = new Gson();
                            JSONArray jsonArray = response.optJSONArray("results");
                            if (jsonArray != null && jsonArray.length() > 0) {

                                try {
                                    JSONObject place = jsonArray.getJSONObject(0);

                                    name = place.getString("name");
                                } catch (JSONException e) {

                                }

                            }
                        }
                        showPopup(currView, location,name);
                    }

                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {

                        showPopup(currView, location, null);

                    }

                });

        // Send request if requestque is available
        if (requestQueue != null) {
            requestQueue.add(jsObjRequest);

        }
    }

    /**
     * Populate map with tasklists from the database
     **/

    public void updateMarkers() {

        IconGenerator generator = new IconGenerator(currContext);
        generator.setBackground(currContext.getDrawable(R.drawable.bottom_rectangle));

        // pass marker
        ContentManager cm = ContentManager.getInstance(currContext);
        ArrayList<TodoList> lists = cm.getAllLists();

        mMap.clear();
        for (TodoList list : lists) {

            generator.setContentView(getInfoContents(list));
            Bitmap icon = generator.makeIcon();

            Location loc = list.getLocation();

            MarkerOptions tp = new MarkerOptions().position(new LatLng(loc.getLatitude(), loc.getLongitude())).title(list.getName()).icon(BitmapDescriptorFactory.fromBitmap(icon));
            mMap.addMarker(tp);

        }
    }


    /**
     * Custom marker info window
     **/

    public View getInfoContents(TodoList list) {
        // Inflate the layouts for the info window, title and snippet.
        View infoWindow = fragmentInflater.inflate(R.layout.fragment_marker_window, null);

        // Populate marker window
        TextView title = ((TextView) infoWindow.findViewById(R.id.map_listName));
        title.setText(list.getName());

        TextView taskcount = ((TextView) infoWindow.findViewById(R.id.map_taskCount));
        taskcount.setText("Unfinished: " + list.getNumberOfOpenTasks());


        TextView taskcountF = ((TextView) infoWindow.findViewById(R.id.map_taskCountF));
        taskcountF.setText("Finished: " + list.getNumberOfFinishedTasks());

        return infoWindow;

    }


    /**
     * On location changed listener
     **/

    @Override
    public void onLocationChanged(Location location) {

        mCurrentLocation = location;

    }



    public void showPopup(View anchorView, LatLng loc, String tag) {

        // Case for undefined tag
        if (tag == null) {
            tag = "Not Defined";
        }

        // Create and show the dialog.
        DialogFragment newFragment = NewListDialog.newInstance(loc.latitude, loc.longitude, tag);
        newFragment.setTargetFragment(this, 1);
        newFragment.show(this.getFragmentManager(), null);
    }

    /**
     * Catch dialogbox results
     **/

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == 1) {
            if (resultCode == 1) {
                    // Show the newly saved list
                    updateMarkers();
                }
        }
    }


    /**
     * Map view functions
     **/

    @Override
    public void onResume() {
        mapView.onResume();
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    /**
     * Stop Google api and request que
     **/

    @Override
    public void onDestroy() {
        super.onDestroy();

        MainActivity activity = (MainActivity)getActivity();
        if(mCurrentLocation!= null){
             activity.loc = mCurrentLocation;
        }

        if(mCameraPosition != null){
                activity.camPos = mCameraPosition;
        }

        mapView.onDestroy();
        mGoogleApiClient.disconnect();

        if (requestQueue != null) {
            requestQueue.stop();
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {

        void onFragmentInteraction(Uri uri);
    }
}

