package groupofthree.taskrabbit.entities.location;

import android.location.Location;

/**
 * Created by kyriakos on 29/11/16.
 */

public class LocationWithTag extends Location {

    private String tag;

    public LocationWithTag(String provider, String tag){
        super(provider);
        this.tag = tag;
    }

    public LocationWithTag(String tag){
        super("");
        this.tag = tag;
    }

    public String getTag(){
        return this.tag;
    }

    public void setTag(String tag){
        this.tag = tag;
    }
}
