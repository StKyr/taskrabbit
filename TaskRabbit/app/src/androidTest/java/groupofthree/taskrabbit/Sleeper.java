package groupofthree.taskrabbit;

/**
 * Created by usi on 12/12/16.
 */

public class Sleeper extends Thread {

    private long milliseconds;

    Sleeper(long milliseconds){
        this.milliseconds = milliseconds;
    }

    public void sleep() throws InterruptedException{
        Thread.sleep(milliseconds);
    }
}
