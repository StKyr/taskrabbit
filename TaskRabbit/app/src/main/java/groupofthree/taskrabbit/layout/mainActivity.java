package groupofthree.taskrabbit.layout;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import java.util.List;

import groupofthree.taskrabbit.GeoFencingMonitorService;
import groupofthree.taskrabbit.R;
import groupofthree.taskrabbit.Seeder;
import groupofthree.taskrabbit.content.ContentManager;
import groupofthree.taskrabbit.layout.statistics.StatisticsTab;
import groupofthree.taskrabbit.utils.Util;

/**
 Created by Fabian Ramelsberger, accelerometer code by Kyriakos Stylianopoulos
 **/

public class MainActivity extends FragmentActivity
        implements TaskListFragment.OnFragmentInteractionListener,MapFragment.OnFragmentInteractionListener, SensorEventListener {

    private static int ACTIVE_FRAGMENT = 0;
    private static final int FRAGMENT_DASHBOARD = 0;
    private static final int FRAGMENT_MAP = 1;
    private static final int FRAGMENT_STATISTICS = 2;


    private TaskListFragment listFragment;
    private FloatingActionButton fab;

    //Variables to replace the fragment
    private ImageButton dashboardNavButton;
    private ImageButton mapNavButton;
    private ImageButton statisticsNavButton;
    private int LAST_FRAGMENT_ID;
    private TaskListFragment taskListFragment;
    private MapFragment mapFragment;
    private TodoListTap taskListTap;
    private StatisticsTab statisticsTab;
    private ToggleButton toggleBellSwitch;

    //DistractionFreeMode Variables
    private SensorManager sensorManager;
    private Sensor sensor;
    private int samplesFound;
    private float samplesX[], samplesY[], samplesZ[];
    private SensorEventListener sev = this;
    private boolean userTurnedModeOff = false;
    private AlertDialog alertDialog;
    public boolean inDeletionMapMode=false;


    // Map location variables
    public Location loc;
    public CameraPosition camPos;

    private EditText addTaskBarText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        _startService();
        onInitializeFAB();

        initializeButtons();

        initializeFragmentForTheFirstTime();
        onDistractionFreeInitialization();

        onSwitchToMapFragment(null);

        // Seeder.seed(this);
        // This line of code exists so that to seed the database with data before the in-class presentation
    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(sev);
        Log.d("sensor", "onPause(): unregistering sensor listener");
        samplesFound = 0;

        if (alertDialog != null) alertDialog.dismiss();
    }

    @Override
    protected void onResume() {
        super.onResume();


        // check if location services exist
        int response = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this);
        if (response != ConnectionResult.SUCCESS) {
            GoogleApiAvailability.getInstance().getErrorDialog(this, response, 1).show();
        }

        if (toggleBellSwitch != null && toggleBellSwitch.isChecked())
            distractionFreeMode(true);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    private void onDistractionFreeInitialization() {
        //onDistraction Free initialization
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        samplesX = new float[50];
        samplesY = new float[50];
        samplesZ = new float[50];
    }

    private void initializeFragmentForTheFirstTime() {
        // Create new fragment and transaction
        FragmentManager fragmentManager = getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        taskListTap = new TodoListTap();
        fragmentTransaction.replace(R.id.fragment_container, taskListTap);
        fragmentTransaction.commit();
        LAST_FRAGMENT_ID = R.id.dashboard_list;
    }

    private void initializeButtons() {
        dashboardNavButton = (ImageButton) findViewById(R.id.dashboardFragmentButton);
        mapNavButton = (ImageButton) findViewById(R.id.mapFragmentButton);
        statisticsNavButton = (ImageButton) findViewById(R.id.statisticsFragmentButton);
        toggleBellSwitch = (ToggleButton) findViewById(R.id.toggleBell);
        toggleBellSwitch.setChecked(false);


    }

    private void onInitializeFAB() {
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.hide();
        final MainActivity thisActivity = this;
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!inDeletionMapMode)//
                {
                    inDeletionMapMode=true;
                    fab.getBackground().setColorFilter(getResources().getColor(R.color.colorDelete), PorterDuff.Mode.SRC_ATOP);
                    Toast.makeText(thisActivity, "Deletion mode active", Toast.LENGTH_SHORT).show();

                    //deletion active
                }
                else{
                    inDeletionMapMode=false;
                    fab.getBackground().setColorFilter(getResources().getColor(R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
                    Toast.makeText(thisActivity, "Deletion mode deactivated", Toast.LENGTH_SHORT).show();


                }
            }
        });
    }

    private void _startService() {
        if (Build.VERSION.SDK_INT >= 23) {

            requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION}, 100);

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    || ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Location permissions not granted.", Toast.LENGTH_SHORT).show();
            } else {
                startService(new Intent(getBaseContext(), GeoFencingMonitorService.class));

            }
        } else {
            startService(new Intent(getBaseContext(), GeoFencingMonitorService.class));
        }
    }

    public void getListItemFragment(TaskListFragment listItemFragment) {
        this.listFragment = listItemFragment;
    }

    //Methods to navigatie thrue the application -------------------------
    public void onSwitchToStatisticFragment(View view) {
        ACTIVE_FRAGMENT = FRAGMENT_STATISTICS;
        switchNavigation();
        // Create new fragment and transaction
        //if (statisticsTab == null)
            statisticsTab = new StatisticsTab();
        onSwitchFragments(statisticsTab);
        LAST_FRAGMENT_ID = R.id.fragment_statistics_tab;
    }

    public void onSwitchToListFragment(View view) {
        if(ContentManager.getInstance(getBaseContext()).getAllLists().size()>0) {
            ACTIVE_FRAGMENT = FRAGMENT_DASHBOARD;
            switchNavigation();
            // Create new fragment and transaction
            if (taskListTap == null)
                taskListTap = new TodoListTap();
            onSwitchFragments(taskListTap);

            LAST_FRAGMENT_ID = R.id.dashboard_list;
        }else
            Toast.makeText(this, "Please create a new list first!", Toast.LENGTH_SHORT).show();
    }


    public void onSwitchToMapFragment(View view) {
        ACTIVE_FRAGMENT = FRAGMENT_MAP;
        switchNavigation();

        // Create new fragment and transaction
        if (mapFragment == null) {

            // Set deafult pos and zoom

            LatLng pos = new LatLng(46.0110207, 8.957515899999999);
            loc = new Location("Default");
            loc.setLatitude(pos.latitude);
            loc.setLongitude(pos.longitude);
            camPos = CameraPosition.builder().target(pos).zoom(15).build();
            mapFragment = new MapFragment();
        }
        onSwitchFragments(mapFragment);

        LAST_FRAGMENT_ID = R.id.fragment_map_task;
    }

    private void onSwitchFragments(Fragment nextFragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, nextFragment);
        fragmentTransaction.commit();
    }


    private void switchNavigation() {
        switch (ACTIVE_FRAGMENT) {
            case FRAGMENT_DASHBOARD:
                dashboardNavButton.setBackground(getResources().getDrawable(R.drawable.navigation_active));
                mapNavButton.setBackground(getResources().getDrawable(R.drawable.navigation_inactive));
                statisticsNavButton.setBackground(getResources().getDrawable(R.drawable.navigation_inactive));
                fab.hide();
                break;
            case FRAGMENT_MAP:
                dashboardNavButton.setBackground(getResources().getDrawable(R.drawable.navigation_inactive));
                mapNavButton.setBackground(getResources().getDrawable(R.drawable.navigation_active));
                statisticsNavButton.setBackground(getResources().getDrawable(R.drawable.navigation_inactive));
                fab.show();
                break;
            case FRAGMENT_STATISTICS:
                dashboardNavButton.setBackground(getResources().getDrawable(R.drawable.navigation_inactive));
                mapNavButton.setBackground(getResources().getDrawable(R.drawable.navigation_inactive));
                statisticsNavButton.setBackground(getResources().getDrawable(R.drawable.navigation_active));
                fab.hide();
                break;
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    // on Click event Handler on a single task
    public void onClickDeleteTask(View view) {
        Toast.makeText(this, "Delete Task!", Toast.LENGTH_SHORT).show();
        FragmentManager fragmentManger = getSupportFragmentManager();
        List<Fragment> fragments = fragmentManger.getFragments();
        fragments.size();

        Fragment todoTap = fragments.get(0);
        TaskListFragment taskListFragment = (TaskListFragment) todoTap
                .getChildFragmentManager().findFragmentById(R.id.fragment_list_item);
        taskListFragment.onDeleteIcon(view);
    }

    public void onAddNewTodo(View view) {
        addTaskBarText=(EditText) findViewById(R.id.EditEmptyTodo);
        if(addTaskBarText!=null&&addTaskBarText.getText().toString().equals("")){
            Toast.makeText(this, "Please enter a name for the task", Toast.LENGTH_SHORT).show();
        }else {
            FragmentManager fragmentManger = getSupportFragmentManager();
            List<Fragment> fragments = fragmentManger.getFragments();
            Fragment todoTap = fragments.get(0);
            TaskListFragment taskListFragment = (TaskListFragment) todoTap
                    .getChildFragmentManager().findFragmentById(R.id.fragment_list_item);
            String string = taskListTap.getEditTextString().getText().toString();
            taskListTap.getEditTextString().getText().clear();
            taskListFragment.onCreateNewTodoItem(string);

            // Check if no view has focus:
            View currentFocusView = this.getCurrentFocus();
            if (view != null) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(currentFocusView.getWindowToken(), 0);
            }
        }
    }



    public void onActivateDistractionFreeMode(View view) {
        distractionFreeMode(toggleBellSwitch.isChecked());
    }

    private void distractionFreeMode(boolean isActivated) {
        if (isActivated) {
            new Thread("wait until resume monitoring") {
                @Override
                public void run() {

                    try {
                        Thread.sleep(3000);
                        // try to wait for 3 secs
                    } catch (InterruptedException e) {
                        Log.d("sensor", "3 interrupted");
                    }
                    sensorManager.registerListener(sev, sensor, SensorManager.SENSOR_DELAY_NORMAL);
                }
            }.start();

            samplesFound = 0;
            Toast.makeText(this, "Distraction Free mode activated ", Toast.LENGTH_SHORT).show();

        } else {
            samplesFound = 0;
            sensorManager.unregisterListener(sev);
            Toast.makeText(this, "Distraction Free mode deactivated", Toast.LENGTH_SHORT).show();
        }
    }


    /*
     The idea about packing accelerometer values into groups of samples
     and checking if their variance is above a threshold
     after a number of them is collected
     so that to determine if the device is moving
     was found in StackOverflow (http://stackoverflow.com/questions/14952373/determine-if-device-has-moved)
     but the code was implemented from scratch.
     (Questions and answers in StackOverflow are under the Creative Commons license).
     */

    @Override
    public void onSensorChanged(SensorEvent event) {

        /*
        Business Logic:
        This method is called every time accelerometer data change.
        It stores values to arrays, and every time `_samplesToCollect_` samples are collected,
        it computes the variance of those values.
        If each variance is above `_threshold_`, sample data differ a lot,
        so it can be inferred that the device is moving.
        */


        final float _threshold_ = 2;
        final int _samplesToCollect_ = 50;

        userTurnedModeOff = false;
        samplesFound++;

        if (samplesFound <= _samplesToCollect_) {

            samplesX[samplesFound - 1] = event.values[0];
            samplesY[samplesFound - 1] = event.values[1];
            samplesZ[samplesFound - 1] = event.values[2];

        }

        if (samplesFound == _samplesToCollect_) {

            double varX = Util.variance(samplesX);
            double varY = Util.variance(samplesY);
            double varZ = Util.variance(samplesZ);

            if (varX > _threshold_ || varY > _threshold_ || varZ > _threshold_) {

                sensorManager.unregisterListener(sev);

                alertDialog = new AlertDialog.Builder(this)
                        .setTitle("Stay concentrated")
                        .setMessage("To exit Distraction Free mode click on the bell.")
                        .setPositiveButton(R.string.ok_answer, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {

                                new Thread("wait until restart monitoring") {
                                    @Override
                                    public void run() {

                                        try {
                                            Thread.sleep(3000); // try to wait for 3 secs
                                        } catch (InterruptedException e) {
                                        }
                                        if(toggleBellSwitch.isChecked()) {
                                            sensorManager.registerListener(sev, sensor, SensorManager.SENSOR_DELAY_NORMAL);
                                        }
                                    }
                                }.start();
                            }
                        })
                        .show();


            }

            samplesFound = 0;
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        //nothing to do here
    }


    public void updateCurrentList(Marker marker, float distanceToLocation) {
        ACTIVE_FRAGMENT = FRAGMENT_DASHBOARD;
        switchNavigation();
        // Create new fragment and transaction
        if (taskListTap == null)
            taskListTap = new TodoListTap();

        onSwitchFragments(taskListTap);
        taskListTap.updateInnerList(marker,distanceToLocation);


        LAST_FRAGMENT_ID = R.id.dashboard_list;


    }

}
