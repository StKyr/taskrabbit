package groupofthree.taskrabbit; /**
 * Created by kyriakos on 20/12/16.
 */


import android.content.Context;
import android.location.Location;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

import groupofthree.taskrabbit.content.ContentManager;
import groupofthree.taskrabbit.entities.Task;
import groupofthree.taskrabbit.entities.TodoList;

/**
 * Dummy class to seed the app with content for the in-class presentation
 */
public class Seeder {
    private static ContentManager contentManager;

    private static TodoList list1;
    private static Location location1;
    private static Task t11;
    private static Task t12;
    private static Task t13;
    private static Task t14;
    private static Task t15;

    private static TodoList list2;
    private static Location location2;
    private static Task t21;
    private static Task t22;
    private static Task t23;
    private static Task t24;

    private static TodoList list3;
    private static Location location3;
    private static Task t31;
    private static Task t32;
    private static Task t33;


    public static void seed(Context context){

        contentManager = ContentManager.getInstance(context);

        clearDataBase();

        initializeContent();

        saveContentTodataBase();

        Toast.makeText(context, "Seeding complete!", Toast.LENGTH_SHORT).show();


    }


    private static void clearDataBase(){

        ArrayList<TodoList> allLists = contentManager.getAllLists();
        for (TodoList list: allLists){
            contentManager.deleteTodoList(list);
        }

        // this not needed
        ArrayList<Task> allTasks = contentManager.getAllTasks();
        for (Task t: allTasks){
            contentManager.deleteTask(t, "");
        }

    }

    private static void initializeContent(){

        Calendar calendar = new GregorianCalendar(2016,12,20);

        location1 = new Location("");
        location1.setLatitude(46.0102823);
        location1.setLongitude(8.9573274);

        list1 = new TodoList(location1, "projects", "usi");
        t11 = new Task("Web Atelier", location1);
        t12 = new Task ("Mobile Computing", location1);
        t13 = new Task("Human Computer Inetraction", location1);
        t14 = new Task ("Computer Graphics", location1);
        t15 = new Task ("AI", location1);

        t11.setFinished(true);
        t12.setFinished(true);
        t13.setFinished(true);
        t14.setFinished(true);

        t12.setDateCompleted(calendar.getTime());
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        t11.setDateCompleted(calendar.getTime());
        t13.setDateCompleted(calendar.getTime());
        calendar.add(Calendar.DAY_OF_MONTH, -3);
        t14.setDateCompleted(calendar.getTime());


        list1.addTask(t11);
        list1.addTask(t12);
        list1.addTask(t13);
        list1.addTask(t14);
        list1.addTask(t15);

        list1.setTimeSpent(455520000);





        calendar = new GregorianCalendar(2016,12,20);

        location2 = new Location("");
        location2.setLatitude(46.0081359);
        location2.setLongitude(8.9569563);

        list2 = new TodoList(location2, "chores", "usiHome");
        t21 = new Task("Laundry", location2);
        t22 = new Task ("Clean Kitchen", location2);
        t23 = new Task("Clean the room", location2);
        t24 = new Task ("Prepare for xmas vacation", location2);

        t21.setFinished(true);
        t22.setFinished(true);

        t21.setDateCompleted(calendar.getTime());
        calendar.add(Calendar.DAY_OF_MONTH, -2);
        t22.setDateCompleted(calendar.getTime());

        list2.addTask(t21);
        list2.addTask(t22);
        list2.addTask(t23);
        list2.addTask(t24);

        list2.setTimeSpent(122640000);





        calendar = new GregorianCalendar(2016,12,20);

        location3 = new Location("");
        location3.setLatitude(46.0064526);
        location3.setLongitude(8.9508146);

        list3 = new TodoList(location3, "shopping", "Migros");
        t31 = new Task("Chocolates", location3);
        t32 = new Task ("souvenirs", location3);
        t33 = new Task("groceries", location3);

        t32.setFinished(true);
        calendar.add(Calendar.DAY_OF_MONTH, -3);
        t32.setDateCompleted(calendar.getTime());

        list3.addTask(t31);
        list3.addTask(t32);
        list3.addTask(t33);

        list3.setTimeSpent(4920000);

    }

    private static void saveContentTodataBase(){

        contentManager.saveTodoList(list1);
        contentManager.saveTodoList(list2);
        contentManager.saveTodoList(list3);
    }
}
