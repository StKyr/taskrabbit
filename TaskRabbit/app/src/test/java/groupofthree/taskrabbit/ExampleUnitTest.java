package groupofthree.taskrabbit;

import org.junit.Test;

import groupofthree.taskrabbit.utils.DurationConverter;
import groupofthree.taskrabbit.utils.MinutesHoursDays;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }

    long oneDayMilliseconds = 86400000;
    long oneAndAHalfDayMilliseconds = oneDayMilliseconds +  oneDayMilliseconds / 2;
    long oneHourMilliseconds = oneDayMilliseconds / 24;
    long halfAnHourMilliseconds = oneHourMilliseconds / 2;

    @Test
    public void TimeConversionMillisecondsToStruct(){



        MinutesHoursDays mhd = DurationConverter.getTimeDuration(oneDayMilliseconds);
        assert (mhd.getDays() == 1 && mhd.getHours() == 0 && mhd.getMinutes() == 0);

        MinutesHoursDays mhd2 = DurationConverter.getTimeDuration(oneAndAHalfDayMilliseconds);
        assert (mhd2.getDays() == 1 && mhd2.getHours() == 12 && mhd2.getMinutes() == 0);

        MinutesHoursDays mhd3 = DurationConverter.getTimeDuration(halfAnHourMilliseconds);
        assert (mhd3.getDays() == 0 && mhd3.getHours() == 0 && mhd3.getMinutes() == 30);

    }

    @Test
    public void StructToMilliseconds(){

        MinutesHoursDays mhd = new MinutesHoursDays(0,0,1);
        assert (mhd.toMilliseconds() == oneDayMilliseconds);

        MinutesHoursDays mhd2 = new MinutesHoursDays(30,12,1);
        assert (mhd2.toMilliseconds() == (oneAndAHalfDayMilliseconds + halfAnHourMilliseconds));

    }






}