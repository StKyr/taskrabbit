package groupofthree.taskrabbit.layout;

import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;

import groupofthree.taskrabbit.R;
import groupofthree.taskrabbit.entities.TodoList;

/**
 * Created by Robert on 19.12.2016.
 */


public class NewListDialog extends DialogFragment {

    double lat;
    double lng;
    String tag;

    groupofthree.taskrabbit.content.ContentManager cm;
    TodoList todolist;
    View currView;

    /**
     *      * Create a new instance of NewListDialog, providing "Longitude, Latitude and a description tag"
     *      * as arguments.
     *      
     */

    static NewListDialog newInstance(double lat, double lng, String tag) {
        NewListDialog f = new NewListDialog();

        // Supply arguments
        Bundle args = new Bundle();
        args.putDouble("lat", lat);
        args.putDouble("lng", lng);
        args.putString("tag", tag);
        f.setArguments(args);

        return f;
    }


    // Get arguments
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.lat = getArguments().getDouble("lat");
        this.lng = getArguments().getDouble("lng");
        this.tag = getArguments().getString("tag");

    }

    // Create dialog
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        currView = inflater.inflate(R.layout.create_list_fragment, container, false);

        // Fill the data in the view
        TextView tagf = (TextView) currView.findViewById(R.id.tagfield);
        tagf.setText("Place: " + tag);

        // Get database
        cm = groupofthree.taskrabbit.content.ContentManager.getInstance(this.getContext());

        // On button click listener
        Button button = (Button) currView.findViewById(R.id.but);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                View tv = currView.findViewById(R.id.nameit);
                String listName = ((EditText) tv).getText().toString();

                // Create new todolist

                Location newLoc = new Location(listName);
                newLoc.setLatitude(lat);
                newLoc.setLongitude(lng);

                todolist = new TodoList(newLoc, listName);
                todolist.setLocationTag(tag);

                // Save list if it does not exist and return
                if (!contains(listName)) {

                    cm.saveTodoList(todolist);
                    getDialog().cancel();

                    getTargetFragment().onActivityResult(1, 1, null);
                } else {
                    ((EditText) tv).setText("Exists");
                }
            }
        });

        return currView;
    }

    // check if database already contains list
    boolean contains(String name) {
        ArrayList<TodoList> lists = cm.getAllLists();
        for (TodoList item : lists) {
            if (item.getName().equals(name)) {
                return true;
            }
        }
        return false;
    }
}


