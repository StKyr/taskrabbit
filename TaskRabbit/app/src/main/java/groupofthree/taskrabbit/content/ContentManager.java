package groupofthree.taskrabbit.content;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import groupofthree.taskrabbit.content.database.DataBaseHelper;
import groupofthree.taskrabbit.entities.TodoList;
import groupofthree.taskrabbit.entities.location.LocationWithTag;
import groupofthree.taskrabbit.entities.Task;
import groupofthree.taskrabbit.utils.DurationConverter;

/**
 * Created by kyriakos on 21/11/16.
 */



/**
 * Use this class to interact with the database. This class implements CRUD actions.
 *
 * Usage:
 * -- initialize/create instance  : ContentManager contentManager = ContentManager.getInstance(getContext());
 * -- get content                 : contentManager.getAllTasks();
 *                                  contentManager.getListsByLocation(43.5680, 8.3449, 1000);
 *                                  ...
 * -- save (new or old) content   : contentManager.saveTask(newTask);
 *                                  ...
 * -- delete content              : contentManager.deleteTodoList(myList);
 *                                  ...
 **/
public class ContentManager {

    // No instance needed. Methods are static

    private static ContentManager _instance;

    public static ContentManager getInstance(Context context){
        if (_instance == null){
            _instance = new ContentManager(context);
        }
        return _instance;
    }

    private ContentManager(Context context){
        init(context);
    }


    private  SQLiteDatabase database;
    private  DataBaseHelper dataBaseHelper;

    /**
     * Initialize connections with the database.
     * Must be called before any other operation.
     * @param context the context to be passed as an argument to the DataBaseHelper -no clue what it does-
     */
    public void init(Context context){
        // for thread safety, there should be only one DataBaseHelper and only one connection to the database

        dataBaseHelper = new DataBaseHelper(context);
        database = dataBaseHelper.getWritableDatabase();

        database.execSQL(DataBaseHelper.CREATE_LOCATION_TABLE_SQL);
        database.execSQL(DataBaseHelper.CREATE_LIST_TABLE_SQL);
        database.execSQL(DataBaseHelper.CREATE_TASK_TABLE_SQL);
    }


    // -- GET --

    /**
     * Given a query that will return the fields of some tasks, return a List of corresponding Task objects
     * @param SQLQuery The sql query to be executed. Fields must be elected in the correct order
     * @return An ArrayList of the tasks parsed
     */
    private  ArrayList<Task> _parseTasks( String SQLQuery) {

        Cursor cursor = database.rawQuery(SQLQuery, null, null);
        ArrayList<Task> list = new ArrayList<>();

        if (cursor.moveToFirst()) {
            do {
                Location location = new Location("");
                location.setLatitude(cursor.getDouble(6));
                location.setLongitude(cursor.getDouble(7));

                Task task = new Task(cursor.getString(0), location);
                task.setDescription(cursor.getString(1));
                task.setTimeSpent(cursor.getLong(5));

                if (cursor.getInt(2) != 0) {
                    task.setFinished(true);
                }else{
                    task.setFinished(false);
                }
                //store dates
                SimpleDateFormat format = new SimpleDateFormat(DataBaseHelper.DATE_FORMAT);
                try {
                    task.setDateCreated(format.parse(cursor.getString(3)));
                    task.setDueDate(format.parse(cursor.getString(4)));
                    if (task.isFinished()) task.setDateCompleted(format.parse(cursor.getString(8)));

                } catch (ParseException e) { // a date was previosuly null
                }

                list.add(task);
                cursor.moveToNext();
            } while (!cursor.isAfterLast());

        }
        cursor.close();
        return list;


    }

    /**
     * Retrieve all existing tasks from the database
     * @return An ArrayList of the Task objects stored
     */
    public  ArrayList<Task> getAllTasks() {

        String sql_getAll = "SELECT " +
                DataBaseHelper.TASK_NAME + ", " +              // 0
                DataBaseHelper.TASK_DESCRIPTION + ", " +       // 1
                DataBaseHelper.TASK_FINISHED + ", " +          // 2
                DataBaseHelper.TASK_DATE_CREATED + ", " +      // 3
                DataBaseHelper.TASK_DUE_DATE + ", " +          // 4
                DataBaseHelper.TASK_TABLE+"."+DataBaseHelper.TASK_TIME_SPENT + ", " +        // 5
                DataBaseHelper.LOCATION_LATITUDE + ", " +      // 6
                DataBaseHelper.LOCATION_LONGITUDE + ", " +     // 7
                DataBaseHelper.TASK_DATE_COMPLETED + " " +     // 8
                "FROM " +
                DataBaseHelper.TASK_TABLE + ", " + DataBaseHelper.LOCATION_TABLE + " " +
                "WHERE " +
                DataBaseHelper.TASK_LOCATION + " = " + DataBaseHelper.LOCATION_TABLE + "."+DataBaseHelper.LOCATION_ID;


        return _parseTasks(sql_getAll);

    }

    /**
     * Retrieve all tasks that exist in a specified distance from a given location
     * @param latitude The latitude coordinate of the location
     * @param longitude The longitude coordinate of the location
     * @param distance The maximum distance (in meters) in which tasks will be returned
     * @return An ArrayList of all the tasks in that area stored in the database
     */
    public  ArrayList<Task> getTasksByLocation( double latitude, double longitude, long distance) {
        ArrayList<Task> allTasks = getAllTasks();
        /*
        It would be more effective to  do the location check in the database
         However, in the database only longitude and latitude are stored (as represented in android.Location class)
         so computing distances there would require definition of SQL function to calculate distances over circles
         which is harder to implement.
         Plus, data in the database are not expected to be much,
         so, the overhead of doing the check in the frontend is neglectable.
         */
        ArrayList<Task> valid = new ArrayList<>();
        Location location = new Location("");
        location.setLatitude(latitude);
        location.setLongitude(longitude);
        for (Task task : allTasks) {
            if (location.distanceTo(task.getLocation()) <= distance) {
                valid.add(task);
            }
        }
        return valid;
    }

    private  ArrayList<Task> _getTasksByList( String name) {

        String sql_getInner = "SELECT " +
                DataBaseHelper.TASK_TABLE+"."+DataBaseHelper.TASK_NAME + ", " +              // 0
                DataBaseHelper.TASK_DESCRIPTION + ", " +       // 1
                DataBaseHelper.TASK_FINISHED + ", " +          // 2
                DataBaseHelper.TASK_DATE_CREATED + ", " +      // 3
                DataBaseHelper.TASK_DUE_DATE + ", " +          // 4
                DataBaseHelper.TASK_TABLE+"."+DataBaseHelper.TASK_TIME_SPENT + ", " +        // 5
                DataBaseHelper.LOCATION_LATITUDE + ", " +      // 6
                DataBaseHelper.LOCATION_LONGITUDE + ", " +     // 7
                DataBaseHelper.TASK_DATE_COMPLETED + " " +     // 8
                "FROM " +
                DataBaseHelper.TASK_TABLE + ", " + DataBaseHelper.LOCATION_TABLE + ", " + DataBaseHelper.LIST_TABLE + " " +
                "WHERE " +
                DataBaseHelper.TASK_LOCATION + " = " + DataBaseHelper.LOCATION_TABLE + "." + DataBaseHelper.LOCATION_ID + " "
                + " AND " + DataBaseHelper.TASK_LIST_BELONGING + " = " + DataBaseHelper.LIST_TABLE + "." + DataBaseHelper.LIST_NAME + " "
                + " AND " + DataBaseHelper.LIST_TABLE+"."+DataBaseHelper.LIST_NAME + " = '"+ name+"' ";

        return _parseTasks( sql_getInner);

    }

    /**
     * Given a query that will return the fields of some lists, return a List of corresponding TodoList objects
     * @param SQLquery The sql query to be executed. Fields must be elected in the correct order
     * @return An ArrayList of the lists parsed
     */

    private  ArrayList<TodoList> _parseLists(String SQLquery) {

        Cursor cursor = database.rawQuery(SQLquery, null, null);
        ArrayList<TodoList> list = new ArrayList<>();

        if (cursor.moveToFirst()) {
            do {
                Location location = new Location("");
                location.setLatitude(cursor.getDouble(2));
                location.setLongitude(cursor.getDouble(3));

                TodoList todoList = new TodoList(location, cursor.getString(0), cursor.getString(1));
                todoList.setTasks(_getTasksByList(todoList.getName()));
                todoList.setTimeSpent(cursor.getLong(4));

                list.add(todoList);
                cursor.moveToNext();
            } while (!cursor.isAfterLast());

        }
        cursor.close();
        return list;
    }

    /**
     * Retrieve all To-do lists stored in the database
     * @return an ArrayList of TodoList objects (with their tasks) found.
     */
    public  ArrayList<TodoList> getAllLists() {

        String sql_getAll = "SELECT " +
                DataBaseHelper.LIST_TABLE+"."+DataBaseHelper.LIST_NAME + ", " +         // 0
                DataBaseHelper.LIST_TABLE+"."+DataBaseHelper.LIST_TAG + ", " +          // 1
                DataBaseHelper.LOCATION_LATITUDE + ", " +                               // 2
                DataBaseHelper.LOCATION_LONGITUDE + ", " +                              // 3
                DataBaseHelper.LIST_TIME_SPENT + " " +                                  // 4
                "FROM " +
                DataBaseHelper.LIST_TABLE + ", " +
                DataBaseHelper.LOCATION_TABLE + " " +
                "WHERE " +
                DataBaseHelper.LIST_LOCATION + " = " + DataBaseHelper.LOCATION_TABLE + "." +DataBaseHelper.LOCATION_ID;

        return _parseLists(sql_getAll);

    }

    public  ArrayList<TodoList> getListsByLocation(double latitude, double longitude, long distance) {
        ArrayList<TodoList> allLists = getAllLists();

        Location location = new Location("");
        location.setLatitude(latitude);
        location.setLongitude(longitude);

        ArrayList<TodoList> valid = new ArrayList<>();
        for (TodoList list : allLists) {
            if (location.distanceTo(list.getLocation()) <= distance) {
                valid.add(list);
            }
        }
        return valid;
    }

    public  ArrayList<LocationWithTag> getAllLocations(){
        ArrayList<LocationWithTag> locations = new ArrayList<>();


        Cursor cursor = database.query(
                DataBaseHelper.LOCATION_TABLE,
                new String[] {
                        DataBaseHelper.LOCATION_LATITUDE,
                        DataBaseHelper.LOCATION_LONGITUDE,
                        DataBaseHelper.LOCATION_TAG},
                null, null, null, null, null
        );

        cursor.moveToFirst();
        while (!cursor.isAfterLast()){
            LocationWithTag loc = new LocationWithTag(cursor.getString(2));
            loc.setLatitude(cursor.getDouble(0));
            loc.setLongitude(cursor.getDouble(1));

            locations.add(loc);
            cursor.moveToNext();
        }
        cursor.close();
        return locations;
    }


    // ---- SAVE ---


    public void saveLocation( Location location, String locationTag){

        String id = location.getLatitude() + "+" + location.getLongitude();
        Cursor cursor = database.query(
                DataBaseHelper.LOCATION_TABLE,
                new String[] {DataBaseHelper.LOCATION_ID},
                DataBaseHelper.LOCATION_ID+" = ? ",
                new String[] {id},
                null, null, null
        );

        if (cursor.getCount() == 0 ) {
            ContentValues values = new ContentValues();
            values.put(DataBaseHelper.LOCATION_ID, id);
            values.put(DataBaseHelper.LOCATION_TAG, locationTag);
            values.put(DataBaseHelper.LOCATION_LATITUDE, location.getLatitude());
            values.put(DataBaseHelper.LOCATION_LONGITUDE, location.getLongitude());

            database.insert( DataBaseHelper.LOCATION_TABLE,null, values);
        }

        cursor.close();

    }




    private boolean checkIfListExists(String listName) {

        Cursor cursor = database.query(                     // query db for list
                DataBaseHelper.LIST_TABLE,
                new String[]{DataBaseHelper.LIST_NAME},
                DataBaseHelper.LIST_NAME + " = ? ",
                new String[]{listName},
                null, null, null, null);

        if (cursor.getCount() > 0) {                         // list exists
            Log.d("stacktrace", "list exists");
            cursor.close();
            return true;

        } else {                                            // list does not exist
            cursor.close();
            return false;
        }
    }

    /**
     * Save a task to the database (create or update). Belonging list should already be saved.
     * @param task The task to be saved.
     * @param listName The name of the list the task belongs to. This list must be already stored.
     * @throws IllegalArgumentException If a list with that name is not stored in the database.
     */
    public  void saveTask( Task task, String listName) throws IllegalArgumentException{
        Log.d("stacktrace", "inside saveTrack()");

        if (!checkIfListExists(listName)) throw new IllegalArgumentException("list "+listName+" does not exist.");

        String locationId = task.getLocation().getLatitude()+"+"+task.getLocation().getLongitude();
        SimpleDateFormat format = new SimpleDateFormat(DataBaseHelper.DATE_FORMAT);
        String taskId = task.getName() + "+" + task.getLocation().getLatitude() + "+" + task.getLocation().getLongitude();
        ContentValues taskValues = new ContentValues();
        taskValues.put(DataBaseHelper.TASK_ID, taskId);
        taskValues.put(DataBaseHelper.TASK_NAME, task.getName());
        taskValues.put(DataBaseHelper.TASK_DESCRIPTION, task.getDescription());
        taskValues.put(DataBaseHelper.TASK_FINISHED, task.isFinished() ? 1 : 0);
        taskValues.put(DataBaseHelper.TASK_DATE_CREATED, (task.getDateCreated() != null) ? format.format(task.getDateCreated()) : "NULL");
        taskValues.put(DataBaseHelper.TASK_DUE_DATE, (task.getDueDate() != null) ? format.format(task.getDueDate()) : "NULL");
        taskValues.put(DataBaseHelper.TASK_TIME_SPENT, DurationConverter.toMilliseconds(task.timeSpent()));
        taskValues.put(DataBaseHelper.TASK_LOCATION, locationId);
        taskValues.put(DataBaseHelper.TASK_LIST_BELONGING, listName);

        Log.d("stacktrace", "inserting task");
        long val = database.insertWithOnConflict(DataBaseHelper.TASK_TABLE, null, taskValues, SQLiteDatabase.CONFLICT_IGNORE);
        if (val == -1){ // already existing
            Log.d("stacktrace", "task exists. updating");
            database.update(
                    DataBaseHelper.TASK_TABLE,
                    taskValues,
                    DataBaseHelper.TASK_NAME+" = ? AND "+DataBaseHelper.TASK_LOCATION+" = ? ",
                    new String[]{task.getName(), locationId}
            );

        }
    }

    /**
     * Saves (create or update) s list in the database. Automatically also saves all of its tasks.
     * @param list The list to be saved in the database
     */
    public void saveTodoList( TodoList list)  {

        saveLocation(list.getLocation(), "");

        String locationId = list.getLocation().getLatitude() + "+" + list.getLocation().getLongitude();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DataBaseHelper.LIST_NAME, list.getName());
        contentValues.put(DataBaseHelper.LIST_LOCATION, locationId);
        contentValues.put(DataBaseHelper.LIST_TIME_SPENT, DurationConverter.toMilliseconds(list.timeSpent()));
        if (list.getLocationTag() != null && list.getLocationTag().equals(""))
            contentValues.put(DataBaseHelper.LIST_TAG, list.getLocationTag());

        long val = database.insertWithOnConflict(DataBaseHelper.LIST_TABLE, null, contentValues, SQLiteDatabase.CONFLICT_IGNORE);
        if (val == -1) { // already existing
            Log.d("stacktrace", "list exists. updating");
            database.update(
                    DataBaseHelper.LIST_TABLE,
                    contentValues,
                    DataBaseHelper.LIST_NAME + " = ? ",
                    new String[]{list.getName()}
            );

        }

        for (Task t : list.getTasks()){
            saveTask(t, list.getName());
        }
    }


    // -- DELETE --

    /**
     * Delete a single task from the database
     * @param task The task to be deleted
     * @param listBelonging Name of the list that task belongs to
     * @return `1` if a single task deleted, `0` if nothing was affected, other value in case of error (number of rows affected)
     */
    public int deleteTask(Task task, String listBelonging){

        return database.delete(
                DataBaseHelper.TASK_TABLE,
                DataBaseHelper.TASK_NAME + " = ? "+
                        "AND " + DataBaseHelper.TASK_LIST_BELONGING + " = ? ",
                new String[] {task.getName(), listBelonging}
        );
    }

    /**
     * Delete a list and all of its tasks
     * @param list The list to be deleted
     * @return `1` if a single list deleted, `0` if nothing was affected, other value in case of error (number of rows affected)
     */
    public int deleteTodoList(TodoList list){

        for (Task task : list.getTasks()){
            int code = deleteTask(task, list.getName());
            if (code != 1) Log.d("stacktrace", "ERROR DELETING TASK OF LIST!");
        }

        return database.delete(
                DataBaseHelper.LIST_TABLE,
                DataBaseHelper.LIST_NAME + " = ? ",
                new String[] { list.getName()}
        );

    }







}
