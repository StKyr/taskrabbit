package groupofthree.taskrabbit.layout;

/**
 Created by Fabian Ramelsberger
 **/

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.Marker;

import java.util.ArrayList;
import java.util.Collections;

import groupofthree.taskrabbit.R;
import groupofthree.taskrabbit.content.ContentManager;
import groupofthree.taskrabbit.entities.Task;
import groupofthree.taskrabbit.entities.TodoList;
import groupofthree.taskrabbit.utils.MinutesHoursDays;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link TaskListFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TaskListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TaskListFragment extends ListFragment {

    private OnFragmentInteractionListener mListener;
    private TaskListAdapter adapter;
    private ContentManager contentManager;
    private TodoList currentTodoList;
    private Context context;
    private EditText lastAddedEditText;
    private boolean onCreateEmptyTask = false;
    private int oldPosition;
    private Marker marker;
    private float distanceToLocation;
    private Button addTaskBarButton;


    public TaskListFragment() {
        // Required empty public constructor
    }

    public void updateBoolean(int position, boolean isChecked) {
        currentTodoList.getTask(position).setFinished(isChecked);
        contentManager.saveTask(currentTodoList.getTask(position), currentTodoList.getName());
        onUpdateList();
    }

    // Container Activity must implement this interface
    public interface OnTaskSelectedListener {
        public void onTaskSelected(String taskName, String taskDescription, String finished);

    }

    public static TaskListFragment newInstance(String param1, String param2) {
        TaskListFragment fragment = new TaskListFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);

        return fragment;
    }

    @SuppressLint("NewApi")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private void onUpdateList() {
        ArrayList<Task> unsortedTodoList = this.currentTodoList.getTasks();
        Collections.sort(unsortedTodoList);
        currentTodoList.setTasks(unsortedTodoList);

        oldPosition = getListView().getFirstVisiblePosition();

        ArrayList<Task> listOfTasks = this.currentTodoList.getTasks();
        ArrayList<String> todoNames = new ArrayList<String>();
        ArrayList<Boolean> todoBoolean = new ArrayList<Boolean>();
        for (int i = 0; i < listOfTasks.size(); i++) {
            todoNames.add(listOfTasks.get(i).getName());
            todoBoolean.add(listOfTasks.get(i).isFinished());
        }

        adapter = new TaskListAdapter(getActivity(),
                todoNames, new ArrayList<String>(), todoBoolean);
        adapter.setTaskListFragment(this);
        setListAdapter(adapter);

        TodoListTap parentFragment = (TodoListTap) getParentFragment();
        String distanceString="";

        if(distanceToLocation!=0){
            String metersString=""+Math.round(distanceToLocation);
            distanceString="Distance: "+metersString+ " meters";
        }

        parentFragment.onSetNameAndDistance(this.currentTodoList.getName(), ""+distanceString);

        //Set information how long you have spend on the current location
        MinutesHoursDays minutesHoursDays = this.currentTodoList.timeSpent();
        String timeSpendString = "";
        if (minutesHoursDays.getDays() > 0)
            timeSpendString += minutesHoursDays.getDays() +
                    " days, ";
        if (minutesHoursDays.getHours() > 0)
            timeSpendString += minutesHoursDays.getHours() + " hours and ";

        timeSpendString += minutesHoursDays.getMinutes() + " minutes spent in the location";

        parentFragment.onSetTimeSpentOn(timeSpendString);

        //save list
        try {
            contentManager.saveTodoList(this.currentTodoList);
            adapter.notifyDataSetChanged();
        } catch (Exception e) {
        }
        getListView().setSelection(oldPosition);
    }


    @Override
    public void onStart() {
        super.onStart();
        marker=((TodoListTap)getParentFragment()).getCurrentMarker();
        distanceToLocation=((TodoListTap)getParentFragment()).getDistanceToLocation();
        ((MainActivity) getActivity()).getListItemFragment(this);
        addTaskBarButton=(Button)getActivity().findViewById(R.id.addTodoButton);
        addTaskBarButton.setEnabled(false);

        contentManager = ContentManager.getInstance(getContext());
        onCreateTodoList();
    }

    public void onDeleteIcon(View view) {
        ListView listview = (ListView) view.getParent().getParent().getParent().getParent();
        final int position = listview.getPositionForView(view);
        onDeleteTask(position);
    }

    private void onDeleteTask(int position) {
        contentManager.deleteTask(currentTodoList.getTask(position), currentTodoList.getName());
        currentTodoList.removeTask(position);
        onUpdateList();
    }


    private void onCreateTodoList() {
        try {

            ArrayList<TodoList> allLists = contentManager.getAllLists();//Create by location
            if (marker==null) {
                currentTodoList = allLists.get(0);
                if(currentTodoList==null){
                    addTaskBarButton.setEnabled(false);
                }
                else {
                    onUpdateList();
                    addTaskBarButton.setEnabled(true);
                }
                    distanceToLocation = 0;
            }else {
                onChangeCurrentList(marker);
                addTaskBarButton.setEnabled(true);
                onUpdateList();
            }
            contentManager.saveTodoList(currentTodoList);
            //Distance & Name on header
        } catch (Exception e) {
        }
    }

    public void onCreateNewTodoItem(String text) {
        if(currentTodoList!=null) {
            addTaskBarButton.setEnabled(true);
            currentTodoList.addTask(new Task(text, currentTodoList.getLocation()));
            onUpdateList();
            getListView().setSelection(0);
        }else
            addTaskBarButton.setEnabled(false);
    }

    public void setTextAndSave(String text, int pos) {
        Task oldTask = currentTodoList.getTask(pos);
        Task newTask = oldTask;
        newTask.setName(text);
        contentManager.deleteTask(oldTask, currentTodoList.getName());
        currentTodoList.addTask(newTask);
        saveInDatabase();
    }

    public void saveInDatabase() {
        try {
            contentManager.saveTodoList(currentTodoList);
        } catch (Exception e) {
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
    }

    public void onChangeCurrentList(Marker marker){
        try {
            ArrayList<TodoList> allLists = contentManager.getAllLists();//Create by location
            for(int i=0;i<allLists.size();i++)
            {
                if(allLists.get(i).getName().equals(marker.getTitle()))
                {
                    currentTodoList = allLists.get(i);
                }
            }
            contentManager.saveTodoList(currentTodoList);
            onUpdateList();
            //Distance & Name on header
        } catch (Exception e) {
        }
    }

}
