package groupofthree.taskrabbit.entities;

import android.location.Location;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import groupofthree.taskrabbit.utils.MinutesHoursDays;

/**
 * Created by usi on 28/11/16.
 */

/**
 * A (location based) To-do list
 */
public class TodoList implements Timerable{

    private Location location;
    private String locationTag;
    private String name;

    private long timeSpent;                     // in milliseconds. Formatting will be done by MinutesHoursDays Class

    private ArrayList<Task> todoItems;

    {
        todoItems = new ArrayList<>();
        timeSpent = 0;
    }

    public TodoList(Location location, String name){

        this.location = location;
        this.name = name;
    }

    public TodoList(Location location, String name, String locationTag){

        this.location = location;
        this.name = name;
        this.locationTag = locationTag;
    }


    public void setName(String name){
        this.name = name;
    }

    public String getName(){
        return this.name;
    }

    public void setLocationTag(String locationTag){
        this.locationTag = locationTag;
    }

    public String getLocationTag(){
        return this.locationTag;
    }

    public void setLocation(Location location){
        this.location = location;
    }

    public void setLocation(int latitude, int longtitude){
        this.location.setLatitude(latitude);
        this.location.setLongitude(longtitude);
    }

    public Location getLocation(){
        return this.location;
    }


    public void addTask(Task task){
        this.todoItems.add(0,task);
    }

    public void removeTask(int position){
        this.todoItems.remove(position);
    }

    public void addAllTasks(ArrayList<Task> tasks){
        this.todoItems.addAll(tasks);
    }

    public void setTasks(ArrayList<Task> tasks){
        this.todoItems = tasks;
    }

    public ArrayList<Task> getTasks(){
        return this.todoItems;
    }

    public Task getTask(int index) throws IllegalArgumentException{
        if (index < 0 ) throw new IllegalArgumentException("negative value for index.");

        if (index > this.todoItems.size()) return null;

        return this.todoItems.get(index);
    }

    /**
     * Returns number of items in the list (same as getNumberOfItems() )
     * @return number of items
     */
    public int size() { return this.todoItems.size();}

    /**
     * Returns number of items in the list (same as size() )
     * @return number of items
     */
    public int getNumberOfItems(){
        return this.todoItems.size();
    }


    public int getNumberOfOpenTasks(){

        int count = 0;
        for (Task task: this.todoItems){
            if (!(task.isFinished())){
                count++;
            }
        }
        return count;
    }

    public int getNumberOfFinishedTasks(){
        return this.todoItems.size() - this.getNumberOfOpenTasks();
    }

    /**
     * Returns if some task(s) has inner tasks.
     * @return <code>true</code> if at least one task of the list has one ore more inner tasks,
     * <code>false</code> otherwise
     */
    public boolean hasInnerTasks(){

        for (Task item : this.todoItems){
            if (item.hasInnerTasks()) return true;
        }
        return false;

    }

    @Override
    public MinutesHoursDays timeSpent(){
        return new MinutesHoursDays(this.timeSpent);
    }

    @Override
    public void setTimeSpent(long time) {
        // override previous time spent
        this.timeSpent = time;
    }

    @Override
    public void addTimeSpent(long time) {

        if (this.timeSpent + time >= this.timeSpent){                   // overflow check
            this.timeSpent += time;
        }else{
            return;                                                     // do nothing
        }
    }

    @Override
    public void setTimeSpent(Date from, Date to) throws IllegalArgumentException {
        if (to.before(from)) throw new IllegalArgumentException("parameter to is earlier than from");

        // override previous time spent
        this.timeSpent = to.getTime() - from.getTime();                 // milliseconds subtraction
    }

    @Override
    public void addTimeSpent(Date from, Date to) throws IllegalArgumentException {
        if (to.before(from)) throw new IllegalArgumentException("parameter to is earlier than from");

        long difference = to.getTime() - from.getTime();

        if (this.timeSpent + difference >= this.timeSpent){             // overflow check
            this.timeSpent += difference;
        }else{
            return;
        }
    }
}
