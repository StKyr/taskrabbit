package groupofthree.taskrabbit.layout;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by kyriakos and Fabian on 14/12/16.
 */


/*
       Source : The code snippet below is copied from WiFun App which was created in class for demonstrating purposes. Minor changes applied.
       Link   : Code can be found in iCorsi slides and AndroidStudio projects.
       License: That code is under GPLv2 license.
       Credits:
            Mobile Computing Group
            Università della Svizzera italiana (USI)

            authors:
            paul baumann (paul.baumann@wsn.tu-darmstadt.de)
            agon bexheti (agon.bexheti@usi.ch)
            silvia santini (silvia.santini@usi.ch)

            last modified (original code): October 2016
*/

public class CollectionPagerAdapter extends FragmentPagerAdapter {

    public CollectionPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    private static int NUMBER_OF_FRAGMENTS = 0;
    @Override
    public Fragment getItem(int position) {
        Fragment fragment;
        switch (position) {
            default: return null;
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
           default: return "";
        }
    }

    @Override
    public int getCount() {
        return NUMBER_OF_FRAGMENTS;
    }
}
