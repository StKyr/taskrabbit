package groupofthree.taskrabbit.utils;

import java.util.concurrent.TimeUnit;

/**
 * Created by usi on 21/11/16.
 */

/**
 * A utility class that converts time intervals from milliseconds to human readable values
 */
public class DurationConverter {

    // no instance needed
    private DurationConverter(){}


    // constants to make conversion to milliseconds code more readable
    // declared here as static class members to prevent computing same values in every function call
    private static final long milliseconds_per_second = 1000;
    private static final long milliseconds_per_minute = 60 * milliseconds_per_second;
    private static final long milliseconds_per_hour   = 60 * milliseconds_per_minute;
    private static final long milliseconds_per_day    = 24 * milliseconds_per_hour;


    /**
     *Convert values of the parameter to milliseconds
     *
     * @param struct A MinutesHoursDays object whose values will be used to convert
     * @return the number of milliseconds corresponding to the values of the input parameter
     */
    public static final long toMilliseconds(MinutesHoursDays struct){

        return  struct.getDays()    * milliseconds_per_day   +
                struct.getHours()   * milliseconds_per_hour  +
                struct.getMinutes() * milliseconds_per_minute;
    }


    /*
       Source : The code snippet below is copied from StackOverflow, though changes are made.
       Link   : http://stackoverflow.com/questions/625433/how-to-convert-milliseconds-to-x-mins-x-seconds-in-java
       License: Everything posted in a StackOverflow thread is under CreativeCommons license.
     */
    /**
     * Convert a millisecond duration to a MinutesHoursDays object.
     *
     * @param millis A duration to convert to an object.
     * @return A MinutesHoursDays object containing the corresponding minutes/hours/days value.
     * @throws IllegalArgumentException When <i> milis</i> is less than zero.
     *
     */

    public static final MinutesHoursDays getTimeDuration(long millis) throws IllegalArgumentException {
        if(millis < 0)
        {
            throw new IllegalArgumentException("Duration must be greater than zero.");
        }

        long days = TimeUnit.MILLISECONDS.toDays(millis);
        millis -= TimeUnit.DAYS.toMillis(days);
        long hours = TimeUnit.MILLISECONDS.toHours(millis);
        millis -= TimeUnit.HOURS.toMillis(hours);
        long minutes = TimeUnit.MILLISECONDS.toMinutes(millis);
        millis -= TimeUnit.MINUTES.toMillis(minutes);
        //long seconds = TimeUnit.MILLISECONDS.toSeconds(millis);

        MinutesHoursDays struct = new MinutesHoursDays((int)minutes, (int)hours, days);
        //assert (struct.toMilliseconds() == millis);                               // invariant check to detect conversion errors
        return struct;

    }
}
