package groupofthree.taskrabbit.entities;

import java.text.SimpleDateFormat;
import java.util.Date;

import groupofthree.taskrabbit.utils.MinutesHoursDays;

/**
 * Created by kyriakos on 20/11/16.
 */


/**
 * Designated behavior for things that are to be "worked".
 */
public interface Workable extends Timerable {

    /**
     * Returns if the object has ended been worked.
     * @return <code>true</code> if the object has ended been worked,
     * <code>false</code> otherwise.
     */
    public boolean isFinished();

    /**
     * sets object's state as `finished` or `unfinished` according to the parameter.
     * @param finished `true` if state is to become `finished`, `false` otherwise.
     * @return `true` if state of the object changed, `false` otherwise.
     */
    public boolean setFinished(boolean finished);




}
