package groupofthree.taskrabbit.entities;

import android.location.Location;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import groupofthree.taskrabbit.utils.MinutesHoursDays;

/**
 * Created by kyriakos on 28/11/16.
 */

/**
 * model of a (location-based) task written in a to-do list
 */
public class Task implements Workable, Listable, Comparable<Task>{

    private String name;
    private String description;

    private boolean finished;

    private Date dateCreated;
    private Date dueDate;
    private Date dateCompleted;

    private long timeSpent;                     // in milliseconds. Formatting will be done by MinutesHoursDays Class

    private Location location;
    private String locationTag;

    private ArrayList<String> innerTasks;

    {
        location = null;
        innerTasks = new ArrayList<>();
        locationTag = "";
        timeSpent = 0;
        finished  = false;
    }

    public Task(String name, Location location){
        this.name = name;
        this.location = location;
    }

    public Task(String name, Date dateCreated, Location location){
        this.name = name;
        this.dateCreated = dateCreated;
        this.location = location;
    }

    public Task(String name, Date dateCreated, Location location, String locationTag){
        this.name = name;
        this.dateCreated = dateCreated;
        this.location = location;
        this.locationTag = locationTag;
    }

    public void setLocation(Location location){
        this.location = location;
    }

    public void setLocationTag(String tag){
        this.locationTag = tag;
    }

    public Location getLocation(){
        return this.location;
    }

    public String getLocationTag(){ return this.locationTag; }

    public Location setLocation(double latitude, double longitude){
        Location loc = new Location("");
        loc.setLatitude(latitude);
        loc.setLongitude(longitude);
        this.location = loc;
        return loc;
    }

    /* -- Listable Interface methods -- */
    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public Date getDateCreated() {
        return this.dateCreated;
    }

    @Override
    public Date getDueDate() {
        return this.dueDate;
    }

    @Override
    public String getDescription() {
        return this.description;
    }

    @Override
    public void setName(String name) { this.name = name; }

    @Override
    public void setDateCreated(Date date) { this.dateCreated = date; }

    @Override
    public void setDueDate(Date date) {
        this.dueDate = date;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }


    /* -- Workable Interface methods -- */

    @Override
    public boolean isFinished() { return this.finished; }

    @Override
    public boolean setFinished(boolean finished) {

        boolean previousState = this.finished;
        this.finished = finished;

        if (finished) this.dateCompleted = new Date();
        else this.dateCompleted = null;

        return !(previousState == finished);

    }

    public Date getDateCompleted(){
        return this.dateCompleted;
    }

    @Override
    public MinutesHoursDays timeSpent(){
        return new MinutesHoursDays(this.timeSpent);
    }

    @Override
    public void setTimeSpent(long time) {
        // override previous time spent
        this.timeSpent = time;
    }

    @Override
    public void addTimeSpent(long time) {

        if (this.timeSpent + time >= this.timeSpent){                   // overflow check
            this.timeSpent += time;
        }else{
            return;                                                     // do nothing
        }
    }

    @Override
    public void setTimeSpent(Date from, Date to) throws IllegalArgumentException {
        if (to.before(from)) throw new IllegalArgumentException("parameter to is earlier than from");

        // override previous time spent
        this.timeSpent = to.getTime() - from.getTime();                 // milliseconds subtraction
    }

    @Override
    public void addTimeSpent(Date from, Date to) throws IllegalArgumentException {
        if (to.before(from)) throw new IllegalArgumentException("parameter to is earlier than from");

        long difference = to.getTime() - from.getTime();

        if (this.timeSpent + difference >= this.timeSpent){             // overflow check
            this.timeSpent += difference;
        }else{
            return;
        }
    }

    public int getNumberOfInnerTasks(){
        return this.innerTasks.size();
    }

    public ArrayList<String> getInnerTasks(){
        return this.innerTasks;
    }

    public String getInnerTask(int index) throws IllegalArgumentException{
        if (index < 0 ) throw new IllegalArgumentException("Negative value as index");

        if (index >= this.innerTasks.size() ) return null;

        return this.innerTasks.get(index);
    }

    public void setInnerTasks(ArrayList<String> tasks){
        this.innerTasks = tasks;
    }

    public void addInnerTask(String task){
        this.innerTasks.add(task);
    }

    public void addAllInnerTasks(ArrayList<String> tasks){
        this.innerTasks.addAll(tasks);
    }

    public boolean hasInnerTasks(){
        return this.innerTasks.size() != 0;
    }

    /**
     * set date of completion
     * @param dateCompleted date of completion
     * @return false if task is not yet finished, true otherwise
     */
    public boolean setDateCompleted(Date dateCompleted){

        if (!this.finished) return false;

        this.dateCompleted = dateCompleted;
        return true;
    }


    @Override
    public int compareTo(Task task) {
        if (this.getDateCreated() == null || task.getDateCreated() == null)
            return 0;
        return getDateCreated().compareTo(task.getDateCreated());
    }

}
