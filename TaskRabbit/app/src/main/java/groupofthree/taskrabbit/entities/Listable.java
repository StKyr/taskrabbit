package groupofthree.taskrabbit.entities;

import java.util.Date;

/**
 * Created by kyriakos on 20/11/16.
 */

/**
 * Behavior for objects that are to be written down in a list
 */
public interface Listable {

    public String getName();

    public Date getDateCreated();

    public Date getDueDate();

    public String getDescription();

    public void setName(String name);

    public void setDateCreated(Date date);

    public void setDueDate(Date date);

    public void setDescription(String description);

}
