package groupofthree.taskrabbit.content.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by kyriakos on 24/11/16.
 */

public class DataBaseHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 10;
    private static final String DATABASE_NAME = "com_groupofthree_taskrabbit_database";

    public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss:SSS";


    public static final String TASK_TABLE = "TASKS";
        public static final String TASK_ID = "id";              // name+latitude+longitude
        public static final String TASK_NAME = "name";
        public static final String TASK_DESCRIPTION = "description";
        public static final String TASK_FINISHED = "finished";
        public static final String TASK_LOCATION = "locationId";
        public static final String TASK_TIME_SPENT = "timeSpent";
        public static final String TASK_DATE_CREATED = "dateCreated";
        public static final String TASK_DUE_DATE = "dueDate";
        public static final String TASK_LIST_BELONGING = "listBelonging";
        public static final String TASK_DATE_COMPLETED = "dateCompleted";

    public static final String LOCATION_TABLE = "LOCATION";
        public static final String LOCATION_ID = "id";
        public static final String LOCATION_LATITUDE = "latitude";
        public static final String LOCATION_LONGITUDE = "longitude";
        public static final String LOCATION_TAG = "tag";

    public static final String LIST_TABLE = "LIST";
        public static final String LIST_NAME = "name";
        public static final String LIST_LOCATION = "location";
        public static final String LIST_TAG = "tag";
        public static final String LIST_TIME_SPENT = "timeSpent";

    public static final String CREATE_TASK_TABLE_SQL = "CREATE TABLE IF NOT EXISTS " + TASK_TABLE + " ( " +
                                                                TASK_ID + " TEXT PRIMARY KEY, " +
                                                                TASK_NAME + " TEXT NOT NULL, " +
                                                                TASK_DESCRIPTION + " TEXT, " +
                                                                TASK_FINISHED + " BOOLEAN DEFAULT 0, " +
                                                                TASK_TIME_SPENT + " LONG NOT NULL, " +
                                                                TASK_DATE_CREATED + " DATETIME," +
                                                                TASK_DUE_DATE + " DATETIME, " +
                                                                TASK_LOCATION + " NOT NULL, " +
                                                                TASK_LIST_BELONGING + ", " +
                                                                TASK_DATE_COMPLETED + ", " +
                                                                "FOREIGN KEY ("+TASK_LOCATION+") REFERENCES " + LOCATION_TABLE + "("+LOCATION_ID+"), " +
                                                                "FOREIGN KEY ("+TASK_LIST_BELONGING+") REFERENCES " + LIST_TABLE + "("+LIST_NAME+") ON DELETE CASCADE " +
                                                            ");";

    public static final String CREATE_LOCATION_TABLE_SQL = "CREATE TABLE IF NOT EXISTS " + LOCATION_TABLE + " ( "+
                                                                LOCATION_ID + " TEXT PRIMARY KEY, "+
                                                                LOCATION_LATITUDE + " DOUBLE, " +
                                                                LOCATION_LONGITUDE + " DOUBLE, " +
                                                                LOCATION_TAG + " TEXT " +
                                                            ");";

    public static final String CREATE_LIST_TABLE_SQL = "CREATE TABLE IF NOT EXISTS " + LIST_TABLE + " ( " +
                                                                LIST_NAME + " TEXT PRIMARY KEY, " +
                                                                LIST_TAG + " TEXT, " +
                                                                LIST_LOCATION + ", " +
                                                                LIST_TIME_SPENT + " LONG NOT NULL, " +
                                                                "FOREIGN KEY ("+LIST_LOCATION +") REFERENCES " + LOCATION_TABLE + "("+LOCATION_ID+") " +
                                                            " );";

    public DataBaseHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(CREATE_LOCATION_TABLE_SQL);
        db.execSQL(CREATE_LIST_TABLE_SQL);
        db.execSQL(CREATE_TASK_TABLE_SQL);


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        String drop1 = "DROP TABLE "+TASK_TABLE;
        String drop2 = "DROP TABLE "+LOCATION_TABLE;
        String drop3 = "DROP TABLE "+LIST_TABLE;


        String CREATE_TASK_TABLE_SQL = "CREATE TABLE " + TASK_TABLE + " ( " +
                TASK_ID + " TEXT PRIMARY KEY, " +
                TASK_NAME + " TEXT NOT NULL, " +
                TASK_DESCRIPTION + " TEXT, " +
                TASK_FINISHED + " BOOLEAN DEFAULT 0, " +
                TASK_TIME_SPENT + " LONG NOT NULL, " +
                TASK_DATE_CREATED + " DATETIME," +
                TASK_DUE_DATE + " DATETIME, " +
                TASK_LOCATION + " NOT NULL, " +
                TASK_LIST_BELONGING + ", " +
                TASK_DATE_COMPLETED + ", " +
                "FOREIGN KEY ("+TASK_LOCATION+") REFERENCES " + LOCATION_TABLE + "("+LOCATION_ID+"), " +
                "FOREIGN KEY ("+TASK_LIST_BELONGING+") REFERENCES " + LIST_TABLE + "("+LIST_NAME+") ON DELETE CASCADE " +
                ");";

        String CREATE_LOCATION_TABLE_SQL = "CREATE TABLE " + LOCATION_TABLE + " ( "+
                LOCATION_ID + " TEXT PRIMARY KEY, "+
                LOCATION_LATITUDE + " DOUBLE, " +
                LOCATION_LONGITUDE + " DOUBLE, " +
                LOCATION_TAG + " TEXT " +
                ");";

         String CREATE_LIST_TABLE_SQL = "CREATE TABLE " + LIST_TABLE + " ( " +
                LIST_NAME + " TEXT PRIMARY KEY, " +
                LIST_TAG + " TEXT, " +
                LIST_LOCATION + ", " +
                 LIST_TIME_SPENT + " LONG NOT NULL, " +
                "FOREIGN KEY ("+LIST_LOCATION +") REFERENCES " + LOCATION_TABLE + "("+LOCATION_ID+") " +
                " );";

        db.execSQL(drop1);
        db.execSQL(drop3);
        db.execSQL(drop2);

        db.execSQL(CREATE_LOCATION_TABLE_SQL);
        db.execSQL(CREATE_LIST_TABLE_SQL);
        db.execSQL(CREATE_TASK_TABLE_SQL);

    }

}