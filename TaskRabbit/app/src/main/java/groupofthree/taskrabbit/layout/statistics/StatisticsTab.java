package groupofthree.taskrabbit.layout.statistics;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

import groupofthree.taskrabbit.R;
import groupofthree.taskrabbit.content.ContentManager;
import groupofthree.taskrabbit.entities.Task;
import groupofthree.taskrabbit.entities.TodoList;
import groupofthree.taskrabbit.utils.DurationConverter;
import groupofthree.taskrabbit.utils.Util;


/*
This piece of code is using MPAndroidChart library
Source: https://github.com/PhilJay/MPAndroidChart
License: Apache License 2.0

Also part of the code was originally from those tutorials on YouTube:
https://www.youtube.com/watch?v=VfLop_oLYU0
https://www.youtube.com/watch?v=pi1tq-bp7uA
https://www.youtube.com/watch?v=H6QxMBI2QH4
License: Standard YouTube License
*/


public class StatisticsTab extends Fragment {

    private PieChart pieChart;
    private BarChart barChart;


    public StatisticsTab() {
        // Required empty public constructor
    }

    public static StatisticsTab newInstance() {
        StatisticsTab fragment = new StatisticsTab();
        fragment.setArguments(null);
        Log.d("piechart", "newInstance()");
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_statistics_tab, container, false);

        pieChart = (PieChart) view.findViewById(R.id.chart);
        setUpPieChart();


        barChart = (BarChart) view.findViewById(R.id.barchart);
        setUpBarChart();


        return view;
    }

    private void setUpPieChart() {


        pieChart.setUsePercentValues(true);
        pieChart.setDescription("");
        pieChart.setDrawHoleEnabled(true);
        pieChart.setHoleColorTransparent(true);
        pieChart.setHoleRadius(5);
        pieChart.setTransparentCircleRadius(10);
        pieChart.setRotationAngle(0);
        pieChart.setRotationEnabled(true);

        addDataToPiechart();

    }

    public void addDataToPiechart() {

        ArrayList<TodoList> allLists = ContentManager.getInstance(getContext()).getAllLists();


        ArrayList<Long> timeValues = new ArrayList<>();
        for (TodoList list : allLists)
            timeValues.add(DurationConverter.toMilliseconds(list.timeSpent()));

        ArrayList<Float> percentages = Util.calculatePercentages(timeValues);

        ArrayList<Entry> entries = new ArrayList<>();
        for (int i = 0; i < percentages.size(); i++) {
            entries.add(new Entry(percentages.get(i), i));
        }

        PieDataSet dataSet = new PieDataSet(entries, "Existing todo lists");
        dataSet.setSliceSpace(0);
        dataSet.setSelectionShift(5);


        ArrayList<Integer> colors = new ArrayList<>();

        for (int c : ColorTemplate.PASTEL_COLORS) colors.add(c);
        for (int c : ColorTemplate.VORDIPLOM_COLORS) colors.add(c);
        for (int c : ColorTemplate.COLORFUL_COLORS) colors.add(c);
        colors = Util.randomiseElementsOrder(colors);
        colors.add(0, Color.RED);
        colors.add(1, Color.GREEN);
        colors.add(2, Color.BLUE);
        colors.add(3, Color.YELLOW);
        colors.add(4, Color.CYAN);
        colors.add(5, Color.MAGENTA);

        dataSet.setColors(colors);
        ArrayList<String> listNames = new ArrayList<>();
        for (int i = 0; i < allLists.size(); i++) {
            listNames.add(allLists.get(i).getName());
        }

        PieData data = new PieData(listNames, dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(11);
        data.setValueTextColor(Color.BLACK);


        pieChart.setData(data);
        pieChart.highlightValues(null);
        pieChart.invalidate();

    }

    public void setUpBarChart() {


        ArrayList<Date> dates;
        ArrayList<String> xVals = new ArrayList<>(7);

        ArrayList<Long> yVals;
        ArrayList<BarEntry> barEntries = new ArrayList<>(7);


        Calendar calendar = Calendar.getInstance();
        Date today = calendar.getTime();

        calendar.add(Calendar.DAY_OF_MONTH, -6);
        Date sixDaysBefore = calendar.getTime();

        dates = getDaysList(sixDaysBefore, today);


        yVals = setNumberOfTasksCompletedPerDay(dates);

        for (int i = 0; i < 7; i++) {
            barEntries.add(new BarEntry(yVals.get(i), i));
        }

        SimpleDateFormat sdf = new SimpleDateFormat("E dd/MM");
        for (int i = 0; i < 7; i++) {
            xVals.add(sdf.format(dates.get(i)));
        }



        BarDataSet dataSet = new BarDataSet(barEntries, "");
        dataSet.setBarSpacePercent(20);
        BarData barData = new BarData(xVals, dataSet);
        barData.setDrawValues(true);
        barChart.setData(barData);
        barChart.setDescription("");

    }


    private ArrayList<Date> getDaysList(Date start, Date stop) {

        ArrayList<Date> days = new ArrayList<>();

        Calendar calendar = Calendar.getInstance();
        calendar.clear();
        calendar.setTime(start);

        while (calendar.getTime().before(stop)) {
            days.add(calendar.getTime());
            calendar.add(Calendar.DAY_OF_MONTH, 1);
        }
        days.add(stop);

        return days;
    }

    private ArrayList<Long> setNumberOfTasksCompletedPerDay(ArrayList<Date> days) {


        ArrayList<Long> vals = new ArrayList<>();
        ArrayList<Task> tasks = ContentManager.getInstance(getContext()).getAllTasks();

        SimpleDateFormat sdf = new SimpleDateFormat("dd MM");


        for (int i = 0; i < days.size(); i++) {
            vals.add(0L);


            for (Task t : tasks) {

                if (t.isFinished() && sdf.format(t.getDateCompleted()).equals(sdf.format(days.get(i)))) {
                    vals.add(i, vals.get(i) + 1);
                }

            }
        }

        return vals;
    }


}
