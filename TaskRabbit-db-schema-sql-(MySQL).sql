-- MySQL Script generated by MySQL Workbench
-- Wed Nov 23 21:38:02 2016
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`LOCATION`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`LOCATION` (
  `latitude` DOUBLE NOT NULL,
  `longitude` DOUBLE NOT NULL,
  `tag` VARCHAR(100) NULL,
  PRIMARY KEY (`latitude`, `longitude`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`LIST`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`LIST` (
  `name` VARCHAR(45) NOT NULL,
  `locationTag` VARCHAR(45) NULL,
  `locationLatitude` DOUBLE NOT NULL,
  `locationLongitude` DOUBLE NOT NULL,
  PRIMARY KEY (`name`, `locationLatitude`, `locationLongitude`),
  INDEX `fk_LIST_LOCATION1_idx` (`locationLatitude` ASC),
  INDEX `fk_LIST_LOCATION2_idx` (`locationLongitude` ASC),
  CONSTRAINT `fk_LIST_LOCATION1`
    FOREIGN KEY (`locationLatitude`)
    REFERENCES `mydb`.`LOCATION` (`latitude`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_LIST_LOCATION2`
    FOREIGN KEY (`locationLongitude`)
    REFERENCES `mydb`.`LOCATION` (`longitude`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`TASKS`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`TASKS` (
  `name` VARCHAR(45) NOT NULL,
  `description` VARCHAR(100) NULL,
  `finished` TINYINT(1) NOT NULL DEFAULT 0,
  `dateCreated` DATETIME NULL,
  `dueDate` DATETIME NULL,
  `timeSpent` BIGINT(20) NOT NULL DEFAULT 0,
  `locationLatitude` DOUBLE NOT NULL,
  `locationLongitude` VARCHAR(45) NOT NULL,
  `listBelonging` VARCHAR(45) NULL,
  PRIMARY KEY (`name`, `locationLongitude`, `locationLatitude`),
  INDEX `fk_TASKS_LOCATION1_idx` (`locationLatitude` ASC),
  INDEX `fk_TASKS_LOCATION2_idx` (`locationLongitude` ASC),
  INDEX `fk_TASKS_LIST1_idx` (`listBelonging` ASC),
  CONSTRAINT `fk_TASKS_LOCATION1`
    FOREIGN KEY (`locationLatitude`)
    REFERENCES `mydb`.`LOCATION` (`latitude`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_TASKS_LOCATION2`
    FOREIGN KEY (`locationLongitude`)
    REFERENCES `mydb`.`LOCATION` (`longitude`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_TASKS_LIST1`
    FOREIGN KEY (`listBelonging`)
    REFERENCES `mydb`.`LIST` (`name`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`innerTasks`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`innerTasks` (
  `innerName` VARCHAR(45) NOT NULL,
  `innerLocationLatitude` DOUBLE NOT NULL,
  `innerLocationLongitude` DOUBLE NOT NULL,
  `outerName` VARCHAR(45) NOT NULL,
  `ouerLocationLatitude` VARCHAR(45) NOT NULL,
  `outerLocationLongitude` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`innerName`, `innerLocationLatitude`, `innerLocationLongitude`),
  INDEX `fk_innerTasks_TASKS1_idx` (`innerLocationLatitude` ASC),
  INDEX `fk_innerTasks_TASKS2_idx` (`innerLocationLongitude` ASC),
  INDEX `fk_innerTasks_TASKS3_idx` (`outerName` ASC),
  INDEX `fk_innerTasks_TASKS4_idx` (`ouerLocationLatitude` ASC),
  INDEX `fk_innerTasks_TASKS5_idx` (`outerLocationLongitude` ASC),
  CONSTRAINT `fk_innerTasks_TASKS`
    FOREIGN KEY (`innerName`)
    REFERENCES `mydb`.`TASKS` (`name`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_innerTasks_TASKS1`
    FOREIGN KEY (`innerLocationLatitude`)
    REFERENCES `mydb`.`TASKS` (`locationLatitude`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_innerTasks_TASKS2`
    FOREIGN KEY (`innerLocationLongitude`)
    REFERENCES `mydb`.`TASKS` (`locationLongitude`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_innerTasks_TASKS3`
    FOREIGN KEY (`outerName`)
    REFERENCES `mydb`.`TASKS` (`name`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_innerTasks_TASKS4`
    FOREIGN KEY (`ouerLocationLatitude`)
    REFERENCES `mydb`.`TASKS` (`name`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_innerTasks_TASKS5`
    FOREIGN KEY (`outerLocationLongitude`)
    REFERENCES `mydb`.`TASKS` (`name`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
