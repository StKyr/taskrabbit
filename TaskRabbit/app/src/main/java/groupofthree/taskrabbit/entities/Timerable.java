package groupofthree.taskrabbit.entities;

import java.util.Date;

import groupofthree.taskrabbit.utils.MinutesHoursDays;

/**
 * Created by usi on 10/12/16.
 */

/**
 * Behavior of objects that allow to spent time on.
 */
public interface Timerable {

    /**
     * Returns total time spent working on the object so far.
     * @return A MinutesHoursDays object storing the work time spent on the object.
     *
     */
    public MinutesHoursDays timeSpent();

    /**
     * sets total time spent on the object. This method will <b>override</b> previous saved time spent.
     * @param time total time spent (in milliseconds)
     */
    public void setTimeSpent(long time);

    /**
     * adds to total time spent on the object. This method will <b>not override</b> previous saved time spent.
     * @param time extra time spent (in milliseconds)
     */
    public void addTimeSpent(long time);

    /**
     * sets total time spent on the object. This method will <b>override</b> previous saved time spent.
     * @param from starting date of work
     * @param to ending date of work
     * @throws IllegalArgumentException if <code>to</code> precedes chronologically <code>from</code>.
     */
    public void setTimeSpent(Date from, Date to) throws IllegalArgumentException;

    /**
     * adds to total time spent on the object. This method will <b>not override</b> previous saved time spent.
     * @param from starting date of work
     * @param to ending date of work
     * @throws IllegalArgumentException if <code>to</code> precedes chronologically <code>from</code>.
     */
    public void addTimeSpent(Date from, Date to) throws IllegalArgumentException;
}
