package groupofthree.taskrabbit.utils;

import java.util.concurrent.TimeUnit;

/**
 * Created by kyriakos on 20/11/16.
 */


/**
 * A bundle for storing minutes, hours and days values as a structure
 */
public class MinutesHoursDays {

    private int minutes;
    private int hours;
    private long days;

    public MinutesHoursDays(int mm, int hh, long dd)throws IllegalArgumentException{
        if ( (mm < 0 || mm >= 60)
          || (hh < 0 || hh >= 24)
          || (dd < 0 ) ) throw new IllegalArgumentException("Parameters must represent real minutes/hours/days values");

        this.minutes = mm;
        this.hours   = hh;
        this.days    = dd;
    }

    /**
     * Create a new MinutesHoursDays Object by converting the input parameter
     * @param milliseconds The number of milliseconds to be converted in minutes/hours/days
     */
    public MinutesHoursDays(long milliseconds){

        MinutesHoursDays mhd = DurationConverter.getTimeDuration(milliseconds);
        this.minutes = mhd.getMinutes();
        this.hours   = mhd.getHours();
        this.days    = mhd.getDays();
    }

    public int getMinutes(){
        return this.minutes;
    }

    public int getHours() {
        return hours;
    }

    public long getDays() {
        return days;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    public void setDays(long days) {
        this.days = days;
    }

    /**
     * String representation of the object
     * @return A string of format mm.hh.dd or m.h.d . If hours or days have a value of 0, their value will not be omitted.
     */
    public String toString(){
        return ""+minutes+"."+hours+"."+days;
    }




    public long toMilliseconds(){
       return DurationConverter.toMilliseconds(this);
    }




}
