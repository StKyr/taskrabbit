package groupofthree.taskrabbit;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.support.test.*;
import android.support.test.BuildConfig;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;

import groupofthree.taskrabbit.content.ContentManager;
import groupofthree.taskrabbit.entities.Task;
import groupofthree.taskrabbit.entities.TodoList;
import groupofthree.taskrabbit.entities.location.LocationWithTag;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 *
 * NOTE: THOSE TESTS WERE MADE FOR A PREVIOUS VERSION OF THE API AND PROBABLY THEY WILL NOT PASS NOW
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {

    private void _assert(boolean expression){
        if (!expression){
            throw new AssertionError();
        }
    }


    ContentManager contentManager;

    Task taskAtUsi;
    Task taskAtUsiHome;

    TodoList listAtUsi;
    TodoList listAtUsiHome;

    Location usi;
    Location usiHome;


    @Before
    public void init(){
        contentManager = ContentManager.getInstance(InstrumentationRegistry.getTargetContext());

        usi = new Location("");
        usi.setLatitude(46.0109603);
        usi.setLongitude(8.9582572);
        taskAtUsi = new Task("Atelier Project", usi);
        listAtUsi = new TodoList(usi, "projects");
        listAtUsi.addTask(taskAtUsi);

        usiHome = new Location("");
        usiHome.setLatitude(46.0079987);
        usiHome.setLongitude(8.9568776);
        taskAtUsiHome = new Task("Clean kitchen", usiHome);
        listAtUsiHome = new TodoList(usiHome, "chores");
        listAtUsiHome.addTask(taskAtUsiHome);

    }

    @Test
    public void checkRemoval(){
        ArrayList<Task> allTasks = contentManager.getAllTasks();
        ArrayList<groupofthree.taskrabbit.entities.TodoList> allLists = contentManager.getAllLists();
        ArrayList<LocationWithTag> allLocations = contentManager.getAllLocations();

        _assert(
                allLists.size() == 0
                && allTasks.size() == 0
                && allLocations.size() == 0
        );
    }

    @Test
    public void nullInsertionCheck() {
        try {
            contentManager.saveTask(null,"");
        } catch (Exception e) {
            //_assert(false);
        }

        try {
            contentManager.saveTodoList(null);
        } catch (Exception e) {
           // _assert(false);
        }

        try {
            contentManager.saveLocation(null, "");
        } catch (Exception e) {
           // _assert(false);
        }

        ArrayList<Task> allTasks = contentManager.getAllTasks();
        ArrayList<groupofthree.taskrabbit.entities.TodoList> allLists = contentManager.getAllLists();
        ArrayList<LocationWithTag> allLocations = contentManager.getAllLocations();

        _assert(allLists.size() == 0
                && allTasks.size() == 0
                && allLocations.size() == 0
        );

    }

    @Test
    public void listInsertion(){


        try{
            contentManager.saveTodoList(listAtUsi);
        }catch(Exception e){
            _assert(false);
        }

        _assert(contentManager.getAllLists().size() == 1
        && contentManager.getAllLocations().size() == 1
        && contentManager.getAllTasks().size() == 1
        );
    }


    @Test
    public void saveLocations(){


        contentManager.saveLocation(usi, "USI");
        contentManager.saveLocation(usiHome, "USIHOME");

        _assert(contentManager.getAllLocations().size() == 2);

        contentManager.saveLocation(usi, "USI");
        contentManager.saveLocation(usiHome, "ANOTHER TAG");

        _assert(contentManager.getAllLocations().size() == 2);
    }

    @Test
    public void saveLists() throws Exception {

        TodoList emptyList = new TodoList(usi, "nothing to do");

        contentManager.saveTodoList(emptyList);

        _assert(contentManager.getAllLists().size() == 1);
        TodoList list = contentManager.getAllLists().get(0);
        Log.d("test", "list name: "+list.getName());
        Log.d("test", "list location: "+list.getLocation().getLatitude()+", "+list.getLocation().getLongitude());
        Log.d("test", "list location tag: "+list.getLocationTag());

    }



}
