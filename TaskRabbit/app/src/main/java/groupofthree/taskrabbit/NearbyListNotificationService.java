package groupofthree.taskrabbit;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import groupofthree.taskrabbit.content.ContentManager;
import groupofthree.taskrabbit.content.DateStorage;
import groupofthree.taskrabbit.entities.TodoList;
import groupofthree.taskrabbit.layout.MainActivity;

/**
 * Created by kyriakos on 08/12/16.
 */


/*
    Code: Code snippet below originated from a tutorial video by Oracle in Youtube. Many changes applied.
    Link: https://www.youtube.com/watch?v=vdYMemD6TxQ
    License: Standard YouTube License
 */

/**
 * Service to be called when an EXIT or ENTER event happens in a geofence.
 */
public class NearbyListNotificationService extends IntentService {


    public static final String TAG = "GeofenceNotificationService";

    public NearbyListNotificationService(){
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent){

        /*
        Get all lists
        Find which is the one that triggered the geofencing event(s)
        (The above could be done by passing the list or its name as extra the intent,
        but I had troubles implementing that, so I switched to this logic)
        If device entered geofence, display "list nearby" notification and start counting time spent
        If device exited geofence, stop counting time and update the database
         */

        GeofencingEvent event = GeofencingEvent.fromIntent(intent);
        if (event.hasError()){
            return;
        }else{

            // get all lists from the database
            ContentManager contentManager = ContentManager.getInstance(getApplicationContext());
            ArrayList<TodoList> allLists = contentManager.getAllLists();

            // get type of transition (integer that represents ENTER, EXIT or DWELL)
            int transition = event.getGeofenceTransition();

            // get all geofences that triggered this transition event
            List<Geofence> geofences = event.getTriggeringGeofences();
            for (Geofence geofence : geofences ){

                // get list that triggered this transition event
                TodoList nearbyList = null;
                String listId = "";

                // find which list triggered the transition
                for (TodoList list : allLists){
                    listId = list.getLocation().getLatitude()+"+"+list.getLocation().getLongitude();
                    if (listId.equals(geofence.getRequestId())){
                        nearbyList = list;
                        break;
                    }
                }
                if (nearbyList == null) return; // this should not happen


                if (transition == Geofence.GEOFENCE_TRANSITION_ENTER){

                    addNotification("Nearby lists", nearbyList.getName()+" list is located near you.");

                    DateStorage.getInstance().put(listId, new Date());

                }else if (transition == Geofence.GEOFENCE_TRANSITION_EXIT){

                    Date dateStarted = DateStorage.getInstance().get(listId);

                    if (dateStarted != null){

                        nearbyList.addTimeSpent(dateStarted, new Date());

                        contentManager.saveTodoList(nearbyList);


                    }else{          // shouldn't go there
                    }
                }
            }
        }

    }

    /**
     * Display a notification that a nearby list exists and on click, open the app and display the list.
     * @param title Title of the notification
     * @param text Text to be displayed nelow the title
     */
    private void addNotification(String title, String text) {
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.notification_icon)
                        .setContentTitle(title)
                        .setContentText(text);


       Intent notificationIntent = new Intent(this, MainActivity.class);
       PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
       builder.setContentIntent(contentIntent);

        // Add as notification
        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(0, builder.build());
    }
}
