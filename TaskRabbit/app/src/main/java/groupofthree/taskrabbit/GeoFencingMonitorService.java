package groupofthree.taskrabbit;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import groupofthree.taskrabbit.content.ContentManager;
import groupofthree.taskrabbit.entities.TodoList;


/**
 * Created by kyriakos on 07/12/16.
 */


/*

    Code: Code snippet below originated from a tutorial video by Oracle in Youtube. Many changes applied.
    Link: https://www.youtube.com/watch?v=vdYMemD6TxQ
    License: Standard YouTube License

 */

/**
 * Service that starts monitoring device locations using Geofences
 */
public class GeoFencingMonitorService extends Service {

    private GoogleApiClient googleApiClient;
    private ContentManager contentManager;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        contentManager = ContentManager.getInstance(getBaseContext());

        final Context serviceContext = this;                                            // create a googleApiClient to access location services
        googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks(){
                    @Override
                    public void onConnected(Bundle bundle){
                        startLocationMonitoring();
                        startGeoFenceMonitoring();
                    }
                    @Override
                    public void onConnectionSuspended(int i){}
                })
                .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {}
                })
                .build();

        googleApiClient.connect();

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }




    /**
     * create a "location request"
     * use "google api client" to get updates for locations changes every 10 seconds
     */
    private void startLocationMonitoring(){
        // The refreshing rate is really slow so that it is as enery eficient as possible.
        // Even though location detection is a part of our app, constant monitoring can be avoided.

        try{
            LocationRequest locationRequest = LocationRequest.create()
                    .setInterval(10000)             // get updates every 10 seconds
                    .setFastestInterval(9000)
                    .setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

            LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    // since we are using geofences, they are handled by a different logic
                    // so we don't need to do anything every time location changes
                    // but we still need to request location updates
                }
           });

        }catch (SecurityException e){}
    }

    /**
     * Handle GeoFences logic:
     *
     * Add a GeoFence with a radius of 100 meters for every list stored.
     * Set up a request, so as every time a geofence is enter or exited, to call another service to handle this event.
     *
     */
    public void startGeoFenceMonitoring(){

        try{
            googleApiClient.connect();

            ArrayList<Geofence> nearbyGeoFences = new ArrayList<>();
            ArrayList<TodoList> allLists = contentManager.getAllLists();

            for (TodoList list : allLists){
                String geofenceId = list.getLocation().getLatitude()+"+"+list.getLocation().getLongitude();
                Geofence geofence = new Geofence.Builder()
                        .setRequestId(geofenceId)
                        .setCircularRegion(list.getLocation().getLatitude(), list.getLocation().getLongitude(), 100)
                        .setNotificationResponsiveness(1000)
                        .setExpirationDuration(Geofence.NEVER_EXPIRE)
                        .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_EXIT)
                        .build();

                nearbyGeoFences.add(geofence);
            }

            if (nearbyGeoFences.size() == 0) return;

            GeofencingRequest geofencingRequest = new GeofencingRequest.Builder()
                    .setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER)
                    .addGeofences(nearbyGeoFences)
                    .build();


            // this intent will call the service to handle actions when geofences are entered or exited
            Intent intent = new Intent(this, NearbyListNotificationService.class);
            PendingIntent pendingIntent = PendingIntent.getService(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

            if (googleApiClient.isConnected()) {

                LocationServices.GeofencingApi.addGeofences(googleApiClient, geofencingRequest, pendingIntent)
                        .setResultCallback(new ResultCallback<Status>() {
                            @Override
                            public void onResult(@NonNull Status status) {
                                // don't do anythinh
                                // This code previously had log messages but we deleted them before submitting
                                if (status.isSuccess()){
                                }else{
                                }
                            }
                        });
            }
        }catch (SecurityException e){}
    }
}
