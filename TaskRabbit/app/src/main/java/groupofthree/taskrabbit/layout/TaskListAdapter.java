package groupofthree.taskrabbit.layout;

import android.content.Context;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import groupofthree.taskrabbit.R;

/**
 * Created by Fabian on 21/11/16.
 */


public class TaskListAdapter extends ArrayAdapter<String> {
    private final Context context;
    private final ArrayList<String> todoTitelList;
    private final ArrayList<Boolean> todoBooleanList;
    private CheckBox todoCheckBox;
    private TaskListFragment taskListFragment;
    private ArrayList<EditText> todoCheckBoxText;

    public TaskListAdapter(Context context, ArrayList<String> todoTitelList, ArrayList<String> todoIsFinishedList, ArrayList<Boolean> todoBooleanList) {
        super(context, -1, todoTitelList);
        this.context = context;
        this.todoTitelList = todoTitelList;
        this.todoBooleanList = todoBooleanList;
        todoCheckBoxText = new ArrayList<>();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.fragment_single_todo, parent, false);

        // Set UI values values
        final EditText todoCheckBoxText = (EditText) rowView.findViewById(R.id.TodoListCheckBoxText);
        todoCheckBoxText.setText(todoTitelList.get(position));

        Button removeButton = (Button) rowView.findViewById(R.id.btn_remove_task);

        if (todoTitelList.get(position).equals("")) {
            removeButton.setClickable(true);
            removeButton.setAlpha(1);
        }

        todoCheckBox = (CheckBox) rowView.findViewById(R.id.TodoListCheckBox);
        todoCheckBox.setChecked(todoBooleanList.get(position));

        final int pos = position;
        todoCheckBox.
                setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                                    @Override
                                                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                                        Toast.makeText(getContext(), "Clicked on boolean " + pos, Toast.LENGTH_SHORT).show();
                                                        taskListFragment.updateBoolean(pos, isChecked);
                                                    }
                                                }
        );


        todoCheckBoxText.
                setOnEditorActionListener(
                        new EditText.OnEditorActionListener() {
                            @Override
                            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                                if (actionId == EditorInfo.IME_ACTION_NEXT ||
                                        actionId == EditorInfo.IME_ACTION_DONE ||
                                        event.getAction() == KeyEvent.ACTION_DOWN &&
                                                event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                                    // the user is done typing.

                                    taskListFragment.setTextAndSave("" + todoCheckBoxText.getText(), pos);

                                    //Hide keyboard
                                    InputMethodManager inputMethodManager =
                                            (InputMethodManager) taskListFragment.getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                                    inputMethodManager.hideSoftInputFromWindow(todoCheckBoxText.getWindowToken(), 0);

                                    return true; // consume.
                                }
                                return false; // pass on to other listeners.
                            }
                        });

        return rowView;
    }

    public void setTaskListFragment(TaskListFragment taskListFragment) {
        this.taskListFragment = taskListFragment;
    }
}
