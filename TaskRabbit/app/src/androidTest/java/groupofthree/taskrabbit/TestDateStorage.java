package groupofthree.taskrabbit;

/**
 * Created by usi on 12/12/16.
 */

import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import org.junit.Before;
import org.junit.Test;

import java.util.Date;

import groupofthree.taskrabbit.content.DateStorage;


import android.util.Log;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Date;

import groupofthree.taskrabbit.content.DateStorage;
import groupofthree.taskrabbit.utils.DurationConverter;
import groupofthree.taskrabbit.utils.MinutesHoursDays;

import static org.junit.Assert.*;

@RunWith(AndroidJUnit4.class)
public class TestDateStorage {

    private void _assert(boolean expression){
        if (!expression) throw new AssertionError();
    }

    @Before
    public void clearStorage(){

        DateStorage.getInstance().clear();
        _assert(DateStorage.getInstance().isEmpty());
    }

    @Test
    public void checkInsertion(){
        DateStorage storage = DateStorage.getInstance();

        Date now_prev = new Date();
        storage.put("now", now_prev);
        _assert(!storage.isEmpty());

        Date now_after = storage.get("now");
        _assert(now_after != null);
        _assert(storage.isEmpty());

        Log.d("test", "previous date: "+now_prev.getTime());
        Log.d("test", "current date: "+now_after.getTime());
    }
}
